volatile int midiendo = 0;
unsigned long tiempo0 = 0;
unsigned long tiempo1 = 0;
unsigned long tiempo2 = 0;
unsigned long tiempoDeclaracion = 0;
unsigned long tiempoEjecucion = 0;

void setup() {
  Serial.begin(9600);
  attachInterrupt( 0, medirTiempo, RISING);
  Serial.println(F("Listo para medir tiempo"));
}

void loop() {
  if (midiendo == 3) {
    tiempoDeclaracion = tiempo1 - tiempo0;
    tiempoEjecucion = tiempo2 - tiempo1;
    Serial.print(F("Declaracion: "));
    Serial.println(tiempoDeclaracion);
    Serial.print(F("Ejecucion: "));
    Serial.println(tiempoEjecucion);
    midiendo = 0;
  }
}

void medirTiempo() {
  if (midiendo == 0) {
    midiendo = 1;
    tiempo0 = micros();
  } else if (midiendo == 1) {
    midiendo = 2;
    tiempo1 = micros();
  } else if (midiendo == 2){
    midiendo = 3;
    tiempo2 = micros();
  }
}

