/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_defuzzify.h"

#include "uF_internal_mem_func.h"
#include "uF_internal_output_variable.h"
#include "uF_internal_snorm.h"
#include "uF_internal_tnorm.h"

#include "uF_config.h"
#include "uF_fixed_point.h"
#include "uF_ufuzzy.h"

#define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_fixed_point_dt uF_dfuzCoG(uF_output_variable_dt *output_var)
{
	uint32_t aux = KIN1_GetCycleCounter();
	extern uint32_t dos;
   uF_fixed_point_dt (*activation) (uF_fixed_point_dt, uF_fixed_point_dt);
   uF_fixed_point_dt (*aggregation) (uF_fixed_point_dt, uF_fixed_point_dt);
   uF_mem_func_dt *mf;
   uF_fixed_point_dt *mem_func_strengths;
   uF_quad_fixed_point_dt value;
   uF_double_fixed_point_dt normalized_value;
   uF_double_fixed_point_dt sum_weight;
   uF_fixed_point_dt i;
   uF_fixed_point_dt wti;
   uF_fixed_point_dt step;
   uint_fast8_t mem_func_index;

   value = 0;
   sum_weight = 0;
   mf = output_var->mf;
   step = output_var->step;
   mem_func_strengths = output_var->mem_func_strengths;
   activation = output_var->activation;
   aggregation = output_var->aggregation;

   for (i = uF_fixpD2n(step, 1); i < uF_FIXP_UNIT; i += step)
   {
      wti = 0;
      for (mem_func_index = 0; mem_func_index < output_var->mem_func_num; mem_func_index++)
      {
         wti = aggregation(wti, activation(mem_func_strengths[mem_func_index], uF_mfunEval(&mf[mem_func_index], i)));
      }
      value += uF_fixpUml(i, wti);
      sum_weight += wti;
   }

   normalized_value = (uF_double_fixed_point_dt) uF_fixpNrm(value);

   /* Perform the division and return the value. */
   dos = dos + KIN1_GetCycleCounter() - aux;
   return (sum_weight != 0) ? uF_fixpDdv(normalized_value, sum_weight) : uF_fixpD2n(uF_FIXP_UNIT, 1);
}

/*==================[end of file]============================================*/
