/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_reader.h"

#include "uF_internal_utilities.h"

#include "uF_error_code.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void uF_rderInit(uF_reader_dt *reader, uint8_t *ptr, uint32_t size)
{
   uF_utilAssert(reader != 0);
   reader->cursor = ptr;
   reader->end = ptr + size;
}

uint8_t *uF_rderGetCursor(uF_reader_dt *reader)
{
   uF_utilAssert(reader != 0);
   return reader->cursor;
}

uF_error_dt uF_rderRead8(uF_reader_dt *reader, int8_t *value)
{
   uF_utilAssert(reader != 0);

   if (reader->cursor >= reader->end)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   *value = (int8_t) reader->cursor[0];
   reader->cursor += 1;
   return uF_ERROR_NONE;
}

uF_error_dt uF_rderRead8u(uF_reader_dt *reader, uint8_t *value)
{
   uF_utilAssert(reader != 0);

   if (reader->cursor >= reader->end)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   *value = reader->cursor[0];
   reader->cursor += 1;
   return uF_ERROR_NONE;
}

uF_error_dt uF_rderRead16(uF_reader_dt *reader, int16_t *value)
{
   uF_utilAssert(reader != 0);

   if (reader->cursor + 1 >= reader->end)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   *value = *((int16_t *) reader->cursor);
   reader->cursor += 2;
   return uF_ERROR_NONE;
}

uF_error_dt uF_rderSkip(uF_reader_dt *reader, int_fast16_t bytes)
{
   uF_utilAssert(reader != 0);

   if (reader->cursor + bytes > reader->end)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   reader->cursor += bytes;
   return uF_ERROR_NONE;
}

/*==================[end of file]============================================*/
