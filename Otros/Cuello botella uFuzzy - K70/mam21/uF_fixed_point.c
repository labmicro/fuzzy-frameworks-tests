/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "uF_fixed_point.h"

#include "uF_config.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

short uF_fixpToInt(uF_fixed_point_dt fp, short range_min, short range_max)
{

   return (short) (((fp * ((long) range_max - range_min)) >> uF_FIXP_FRACTIONAL_BITS) + range_min);
}

uF_fixed_point_dt uF_fixpFromInt(short i, short range_min, short range_max)
{
   long range_dif = ((long) range_max) - range_min;
   long x = ((long) i) - range_min;
   long num = x * uF_FIXP_UNIT;
   return (uF_fixed_point_dt) (num / range_dif);
}

long uF_fixpToLongInt(uF_fixed_point_dt fp, long range_min, long range_max)
{
   long long range_dif = ((long long) range_max) - range_min;
   long long num = fp * range_dif;
   return (long) ((num >> uF_FIXP_FRACTIONAL_BITS) + range_min);
}

uF_fixed_point_dt uF_fixpFromLongInt(long i, long range_min, long range_max)
{
   long long range_dif = ((long long) range_max) - range_min;
   long long x = ((long long) i) - range_min;
   long long num = x * uF_FIXP_UNIT;
   return (uF_fixed_point_dt) (num / range_dif);
}

#if defined(uF_NO_INT64)

uF_double_fixed_point_dt uF_fixpDdv32(uF_double_fixed_point_dt num, uF_double_fixed_point_dt den)
{
   int_fast8_t k = uF_FIXP_FRACTIONAL_BITS;

   while ((num & 0xC0000000) == 0 && --k >= 0)
   {
      num = num << 1;
   }
   while (--k >= 0)
   {
      den = den >> 1;
   }
   return num / den;
}

#endif

/*==================[end of file]============================================*/
