/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_sugeno.h"

#include "uF_internal_input_variable.h"
#include "uF_internal_output_variable.h"
#include "uF_internal_tnorm.h"

#include "uF_fixed_point.h"
#include "uF_ufuzzy.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static uF_fixed_point_dt ProcessInputs(uF_controller_dt *controller, uF_output_variable_dt *output_var,
      uF_fixed_point_dt *is)
{
   uF_ruleset_dt *rs;
   uF_fixed_point_dt *input_weights;
   uint_fast16_t *input_offset;
   /* Array with the membership function identifier associated with each input for a given rule. */
   int8_t *mem_func_id;
   uint8_t *connective_type;
   uF_fixed_point_dt (*connective)(uF_fixed_point_dt, uF_fixed_point_dt);
   /* Array with output function coefficients. */
   int16_t *coefficients;
   uF_double_fixed_point_dt weight_sum;
   uF_quad_fixed_point_dt value_sum;
   uF_double_fixed_point_dt normalized_value_sum;
   /* Holds the output value. */
   uF_quad_fixed_point_dt output_value;
   uF_double_fixed_point_dt normalized_output_value;
   uF_double_fixed_point_dt ret;
   uF_fixed_point_dt weight;
   uF_fixed_point_dt weight_limit;
   uF_fixed_point_dt tmp;
   uint_fast8_t input_num;
   uint_fast8_t i, j;

   rs = &output_var->ruleset;
   input_weights = controller->input_weights;
   input_num = uF_ufuzInputNum(controller);

   weight_sum = 0;
   value_sum = 0;

   /* For each rule. */
   for (i = 0; i < rs->rule_num; ++i)
   {
      /* Pointer to the membership function id for each term of the antecedent. */
      connective_type = rs->data + i * rs->rule_size;
      mem_func_id = (int8_t *) (connective_type + 1);
      /* Pointer to the coefficients for each term of the consequent. */
      coefficients = (int16_t *) (connective_type + rs->antecedent_size);


      switch (*connective_type)
      {
         case uF_RSET_CONNECTIVE_AND:
            connective = controller->and_op;
            /* Initialize the calculated weight for the rule with logical 1. */
            weight = uF_FIXP_UNIT;
            weight_limit = 0;
            break;
         default:
            connective = controller->or_op;
            /* Initialize the calculated weight for the rule with logical 0. */
            weight = 0;
            weight_limit = uF_FIXP_UNIT;
      }

      /* Assign to temporary variable. */
      input_offset = controller->input_offsets;

      /* Initialize the output value with the independent term. */
      output_value = uF_fixpUml(uF_utilNtohs(*(coefficients++)), uF_FIXP_UNIT);

      /* For each input variable. */
      for (j = 0; j < input_num; ++j, ++mem_func_id, ++input_offset, ++coefficients)
      {
         /* Calculate the partial output weight. */

         /* If the input variable appears in the rule. */
         if (weight != weight_limit && *mem_func_id != 0)
         {
            /* Then accumulate its weight. */

            /* If the value is positive. */
            if(*mem_func_id > 0)
            {
               /* Then use its weight directly. */
               weight = connective(weight, input_weights[*mem_func_id - 1 + *input_offset]);
            }
            else
            {
               /* Else use the not operator. */
               weight = connective(weight, uF_FIXP_UNIT - input_weights[-*mem_func_id - 1 + *input_offset]);
            }
         }

         /* Calculate the partial output value. */
         if (*coefficients != 0)
         {
            tmp = uF_utilNtohs(*coefficients);
            output_value += uF_fixpUml(is[j], tmp);
         }
      }

      if (weight != 0)
      {
         /* Calculate the unnormalized term. */
         normalized_output_value = uF_fixpNrm(output_value);
         value_sum += uF_fixpUml(normalized_output_value, weight);
         weight_sum += weight;
      }
   }

   if (weight_sum == 0)
   {
      /* Then return 0.5. */
      return uF_fixpD2n(uF_FIXP_UNIT, 1);
   }
   /* Normalize the sum. */
   normalized_value_sum = (uF_double_fixed_point_dt) uF_fixpNrm(value_sum);

   /* Calculate the output. */
   ret = uF_fixpDdv(normalized_value_sum, weight_sum);
   return (uF_fixed_point_dt) ret;
}

/*==================[external functions definition]==========================*/

uF_error_dt uF_sgnoRead(uF_controller_dt *controller, uF_output_variable_dt *output_var, uF_reader_dt *reader)
{
   uF_ruleset_dt *ruleset;
   uint_fast16_t consequent_size;
   uint_fast8_t input_num;
   uint_fast8_t rule_num;
   uint_fast8_t rule;
   uint_fast8_t input;
   uint8_t padding;
   uint8_t tmp;
   int8_t mf_id;

   ruleset = &output_var->ruleset;
   input_num = uF_ufuzInputNum(controller);

   /* Initialize the fields. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   ruleset->rule_num = rule_num = tmp;
   ruleset->data = uF_rderGetCursor(reader);

   /* Calculate the size of the consequent. */
   consequent_size = ((input_num + 1) << 1);

   /* Calculate the size of the rule antecedent. 1 byte for each input variable
    * plus one for the logical connective plus padding if needed. */
   ruleset->antecedent_size = input_num + 1;
   padding = 0;
   if ((ruleset->antecedent_size & 0x01u) != 0)
   {
      ++ruleset->antecedent_size;
      padding = 1;
   }

   /* Calculate the size of each rule in bytes. */
   ruleset->rule_size = ruleset->antecedent_size + consequent_size;

   /* For each rule. */
   for (rule = 0; rule < rule_num; ++rule)
   {
      /* Read the logic connective. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      if (tmp >= uF_RSET_CONNECTIVE_NUM)
      {
         return uF_ERROR_UNKNOWN_LOGICAL_CONNECTIVE;
      }

      /* For each input variable. */
      for (input = 0; input < input_num; ++input)
      {
         /* Read the membership function id. */
         if (uF_rderRead8(reader, &mf_id) != uF_ERROR_NONE)
         {
            return uF_ERROR_READER_OVERFLOW;
         }

         /* Check if it is valid. */
         if (uF_utilAbs(mf_id) > uF_ivarGetMemFuncNum(&controller->inputs[input]))
         {
            return uF_ERROR_INVALID_MEMBERSHIP_FUNCTION_ID;
         }
      }

      /* Add padding if necessary to align to 16 bits. */
      if (padding != 0)
      {
         if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
         {
            return uF_ERROR_READER_OVERFLOW;
         }
         else if (tmp != 0)
         {
            return uF_ERROR_PADDING_NOT_ZERO;
         }
      }

      /* Skip the consequents. */
      if (uF_rderSkip(reader, consequent_size) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
   }
   return uF_ERROR_NONE;
}

uF_fixed_point_dt uF_sgnoProcess(uF_controller_dt* controller, uF_output_variable_dt *output_var, uF_fixed_point_dt *is)
{
   return ProcessInputs(controller, output_var, is);
}

/*==================[end of file]============================================*/
