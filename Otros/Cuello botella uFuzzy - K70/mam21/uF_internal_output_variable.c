/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_output_variable.h"

#include "uF_internal_defuzzify.h"
#include "uF_internal_reader.h"
#include "uF_internal_ruleset.h"
#include "uF_internal_utilities.h"

#include "uF_ufuzzy.h"
#include "uF_error_code.h"
#include "uF_memory_pool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_error_dt uF_ovarRead(uF_output_variable_dt *output_var, uF_controller_dt *controller, uF_reader_dt *reader)
{
   uF_error_dt error;
   int_fast8_t i;
   uint8_t mf_num;
   uint8_t tmp;
   uint8_t inference_type;

   uF_utilAssert(output_var != 0);
   uF_utilAssert(controller != 0);
   uF_utilAssert(reader != 0);

   /* Read the inference type. */
   if (uF_rderRead8u(reader, &inference_type) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (inference_type >= uF_OVAR_INFERENCE_NUM)
   {
      return uF_ERROR_UNKNOWN_INFERENCE_METHOD;
   }
   output_var->inference_type = inference_type;

   /* If sugeno. */
   if (output_var->inference_type == uF_OVAR_INFERENCE_SUGENO)
   {

      return uF_sgnoRead(controller, output_var, reader);
   }
   else
   {
      /* Then read the membership functions. */

      /* Read the membership function count. */
      if (uF_rderRead8u(reader, &mf_num) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      output_var->mf = uF_poolAlloc(mf_num * sizeof(uF_mem_func_dt));
      if (output_var->mf == 0)
      {
         return uF_ERROR_MEMORY_POOL_TOO_SMALL;
      }
      output_var->mem_func_num = mf_num;

      /* For each membership function. */
      for (i = 0; i < mf_num; ++i)
      {
         /* Read it. */
         error = uF_mfunRead(&output_var->mf[i], reader);
         if (error != uF_ERROR_NONE)
         {
            return error;
         }
      }

      /* Read the activation operator. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      output_var->activation = uF_tnrmGet(tmp);
      if (output_var->activation == 0)
      {
         return uF_ERROR_UNKNOWN_ACTIVATION_OPERATOR;
      }

      /* Read the aggregation operator. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      output_var->aggregation = uF_snrmGet(tmp);
      if (output_var->aggregation == 0)
      {
         return uF_ERROR_UNKNOWN_AGGREGATION_OPERATOR;
      }

      /* Read the defuzzification method. */
      if (uF_rderRead8u(reader, &output_var->defuzzification) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      if (output_var->defuzzification >= uF_DFUZ_NUM)
      {
         return uF_ERROR_UNKNOWN_DEFUZZIFICATION_METHOD;
      }

      /* Read the base 2 logarithm of the defuzzification steps. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      if (tmp > uF_FIXP_FRACTIONAL_BITS)
      {
         return uF_ERROR_TOO_MANY_DEFUZZIFICATION_STEPS;
      }
      output_var->step = 1 << (uF_FIXP_FRACTIONAL_BITS - tmp);

      /* Read the reserved field. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }

      /* Allocate the memory for the runtime array. */
      output_var->mem_func_strengths = uF_poolAlloc(mf_num * sizeof(*output_var->mem_func_strengths));
      if (output_var->mem_func_strengths == 0)
      {
         return uF_ERROR_MEMORY_POOL_TOO_SMALL;
      }

      return uF_mamdRead(controller, output_var, reader);
   }
}

uint_fast8_t uF_ovarGetMemFuncNum(uF_output_variable_dt *output_var)
{
   uF_utilAssert(output_var != 0);
   return output_var->mem_func_num;
}

/*==================[end of file]============================================*/
