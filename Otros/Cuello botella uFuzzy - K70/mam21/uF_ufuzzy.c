/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include <stdint.h>

#include "uF_ufuzzy.h"

#include "uF_internal_defuzzify.h"
#include "uF_internal_input_variable.h"
#include "uF_internal_mamdani.h"
#include "uF_internal_mem_func.h"
#include "uF_internal_output_variable.h"
#include "uF_internal_ruleset.h"
#include "uF_internal_snorm.h"
#include "uF_internal_sugeno.h"
#include "uF_internal_tnorm.h"
#include "uF_internal_utilities.h"

#include "uF_config.h"
#include "uF_fixed_point.h"
#include "uF_memory_pool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
static uF_error_dt error_code = uF_ERROR_NONE;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** \brief Fuzzifies the given input state.
 **
 ** Calculates the weights of all the input membership functions and returns
 ** them into the controller input_weights field as an array.
 **
 ** \param controller Pointer to the controller being used.
 ** \param is Array with the input values.
 **/
static void Fuzzify(uF_controller_dt* controller, uF_fixed_point_dt* is)
{
   /* Array with the input variables. */
   uF_input_variable_dt* inputs;
   /* Array with the number of membership functions for each input variable. */
   uint_fast8_t* mem_func_num;
   /* Array with the weights for the given input state for all the membership
    * function for all the input variables. */
   uF_fixed_point_dt* mem_func_weigths;
   /* Array with the membership functions for a given input variable. */
   uF_mem_func_dt* mem_funcs;
   /* Total number of input variables. */
   uint8_t input_num;
   /* Reading of the current input variable. */
   uF_fixed_point_dt input_value;
   /* Index to go through the membership functions array. */
   uint8_t mem_func_index;
   /* Index to go through the array of input variables. */
   uint8_t input_index;

   inputs = controller->inputs;
   mem_func_num = controller->mem_func_num;
   mem_funcs = inputs[0].mf;
   input_num = uF_ufuzInputNum(controller);

   /* Point to the beginning of the output array. */
   mem_func_weigths = controller->input_weights;

   /* Use the first input value. */
   input_value = is[0];

   /* For each input variable. */
   for (input_index = 0, mem_func_index = 0; input_index < input_num;
         ++mem_func_index, ++mem_func_weigths)
   {
      /* Evaluate every membership function. */

      /* If all the membership functions have been evaluated. */
      while (mem_func_index >= mem_func_num[input_index])
      {
         /* Then start with the next input variable. */

         /* If all the input variables have been evaluated. */
         if (++input_index >= input_num)
         {
            /* Then finish. */
            return;
         }
         else
         {
            /* Else start with the first membership function for the
             * current input variable. */
            mem_func_index = 0;

            /* Use the corresponding input value. */
            input_value = is[input_index];

            /* Point to the membership function array of the next input
             * variable. */
            mem_funcs = inputs[input_index].mf;
         }
      }

      /* Evaluate the current membership function with the corresponding input
       * value. */
      *mem_func_weigths = uF_mfunEval(&mem_funcs[mem_func_index], input_value);
   }
}

static uF_fixed_point_dt ProcessActuator(uF_controller_dt* controller, int output_id,
      uF_fixed_point_dt* is)
{
   uF_output_variable_dt* output = &controller->outputs[output_id];

   uF_fixed_point_dt value = 0;

   if (output->inference_type == uF_OVAR_INFERENCE_SUGENO)
   {
      value = uF_sgnoProcess(controller, output, is);
   } else
   {
      value = uF_mamdProcess(controller, output, is);
   }
   return value;
}

static uF_error_dt ReadHeader(uF_controller_dt *controller, uF_reader_dt *reader)
{
   uint8_t input_num;
   uint8_t output_num;
   uint8_t tmp;

   /* Point to the beginning of the data. */
   controller->config = uF_rderGetCursor(reader);

   /* Validate the signature. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (tmp != 'C')
   {
      return uF_ERROR_DATA_SIGNATURE;
   }

   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (tmp != 'F')
   {
      return uF_ERROR_DATA_SIGNATURE;
   }

   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (tmp != 'S')
   {
      return uF_ERROR_DATA_SIGNATURE;
   }

   /* Read the reserved field. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }

   /* Validate the and operator. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   controller->and_op = uF_tnrmGet(tmp);
   if (controller->and_op == 0)
   {
      return uF_ERROR_UNKNOWN_AND_OPERATOR;
   }

   /* Validate the or operator. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   controller->or_op = uF_snrmGet(tmp);
   if (controller->or_op == 0)
   {
      return uF_ERROR_UNKNOWN_OR_OPERATOR;
   }

   /* Read the number of input variables. */
   if (uF_rderRead8u(reader, &input_num) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (input_num == 0)
   {
      return uF_ERROR_NO_INPUT_VARIABLES;
   }

   /* Allocate the memory for the array of input variables. */
   controller->inputs = uF_poolAlloc(input_num * sizeof(uF_input_variable_dt));
   if (controller->inputs == 0)
   {
      return uF_ERROR_MEMORY_POOL_TOO_SMALL;
   }

   /* Read the number of output variables. */
   if (uF_rderRead8u(reader, &output_num) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (output_num == 0)
   {
      return uF_ERROR_NO_OUTPUT_VARIABLES;
   }

   /* Allocate the memory for the array of output variables. */
   controller->outputs = uF_poolAlloc(output_num * sizeof(uF_output_variable_dt));
   if (controller->outputs == 0)
   {
      return uF_ERROR_MEMORY_POOL_TOO_SMALL;
   }
   return uF_ERROR_NONE;
}

/*==================[external functions definition]==========================*/

uF_controller_dt *uF_ufuzInit(uint8_t *controller_data, uint32_t data_size)
{
   uF_controller_dt *controller;
   uint_fast8_t* mem_func_num;
   uint_fast16_t* input_offsets;
   uF_reader_dt reader;
   uint_fast16_t total_mem_funcs;
   uint_fast8_t output_num;
   uint_fast8_t input_num;
   uint_fast8_t i;

   if (controller_data == 0)
   {
      error_code = uF_ERROR_NULL_CFS_DATA;
      return 0;
   }

   uF_poolInit();

   controller = uF_poolAlloc(sizeof(uF_controller_dt));
   if (controller == 0)
   {
      error_code = uF_ERROR_MEMORY_POOL_TOO_SMALL;
      return 0;
   }

   /* Initialize the reader. */
   uF_rderInit(&reader, controller_data, data_size);

   /* Read the header. */
   error_code = ReadHeader(controller, &reader);
   if (error_code != uF_ERROR_NONE)
   {
      return 0;
   }

   /* For each input variable. */
   input_num = uF_ufuzInputNum(controller);
   for (i = 0; i < input_num; ++i)
   {
      error_code = uF_ivarRead(&controller->inputs[i], &reader);
      if (error_code != uF_ERROR_NONE)
      {
         return 0;
      }
   }

   /* For each output variable. */
   output_num = uF_ufuzOutputNum(controller);
   for (i = 0; i < output_num; ++i)
   {
      error_code = uF_ovarRead(&controller->outputs[i], controller, &reader);
      if (error_code != uF_ERROR_NONE)
      {
         return 0;
      }
   }

   /*
    * Initialize the runtime arrays.
    */

   /* Allocate the mem_func_num array. */
   mem_func_num = uF_poolAlloc(input_num * sizeof(*controller->mem_func_num));
   if (mem_func_num == 0)
   {
      error_code = uF_ERROR_MEMORY_POOL_TOO_SMALL;
      return 0;
   }

   /* For each input variable. */
   total_mem_funcs = 0;
   for (i = 0; i < input_num; ++i)
   {
      /* Get its number of membership functions. */
      mem_func_num[i] = uF_ivarGetMemFuncNum(&controller->inputs[i]);
      /* Accumulate the total number of membership functions. */
      total_mem_funcs += mem_func_num[i];
   }
   /* Initialize the membership function number pointer. */
   controller->mem_func_num = mem_func_num;

   /* Allocate memory for the input weights array. */
   controller->input_weights = uF_poolAlloc(total_mem_funcs * sizeof(*controller->input_weights));
   if (controller->input_weights == 0)
   {
      error_code = uF_ERROR_MEMORY_POOL_TOO_SMALL;
      return 0;
   }

   /* Allocate the memory for the input offsets. */
   input_offsets = uF_poolAlloc(input_num * sizeof(*controller->input_offsets));
   if (input_offsets == 0)
   {
      error_code = uF_ERROR_MEMORY_POOL_TOO_SMALL;
      return 0;
   }

   /* Initialize the input offsets. */
   input_offsets[0] = 0;
   for (i = 1; i < input_num; ++i)
   {
      input_offsets[i] = input_offsets[i - 1] + mem_func_num[i - 1];
   }
   /* Initialize the input offsets pointer. */
   controller->input_offsets = input_offsets;

   /* Initialize the error code. */
   controller->err = uF_ERROR_NONE;

   return controller;
}

void uF_ufuzClear(uF_controller_dt* controller)
{
}

uF_error_dt uF_ufuzProcess(uF_controller_dt *controller, uF_fixed_point_dt *inputs, int inputs_size,
      uF_fixed_point_dt *outputs, int outputs_size)
{
   /* Number of output variables. */
   uint_fast8_t output_num;
   /* Output variable id. */
   uint_fast8_t output_id;

   if (controller == 0)
   {
      error_code = uF_ERROR_CONTROLLER_NOT_INITIALIZED;
      return uF_ERROR_CONTROLLER_NOT_INITIALIZED;
   }

   /* Get the number of output variables of the controller. */
   output_num = uF_ufuzOutputNum(controller);

   if (inputs_size < uF_ufuzInputNum(controller))
   {
      controller->err = uF_ERROR_INPUT_ARRAY_TOO_SMALL;
      return uF_ERROR_INPUT_ARRAY_TOO_SMALL;
   }

   if (outputs_size < output_num)
   {
      controller->err = uF_ERROR_OUTPUT_ARRAY_TOO_SMALL;
      return uF_ERROR_OUTPUT_ARRAY_TOO_SMALL;
   }

   /* Fuzzify the inputs. */
   Fuzzify(controller, inputs);

   /* For each output variable. */
   for (output_id = 0; output_id < output_num; ++output_id)
   {
      /* Calculate its value. */
      outputs[output_id] = ProcessActuator(controller, output_id, inputs);
   }

   return controller->err;
}

uF_error_dt uF_ufuzError(uF_controller_dt *controller)
{
   uF_error_dt e;
   if (controller == 0)
   {
      e = error_code;
      error_code = uF_ERROR_NONE;
      return e;
   }
   e = controller->err;
   controller->err = uF_ERROR_NONE;
   return e;
}

/*==================[end of file]============================================*/
