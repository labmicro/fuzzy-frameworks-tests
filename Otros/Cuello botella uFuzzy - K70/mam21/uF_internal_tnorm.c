/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "uF_internal_tnorm.h"

#include "uF_fixed_point.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static uF_tnorm_dt tnorms[] =
{
   uF_tnrmMin,
   uF_tnrmAlgebraic,
   uF_tnrmBounded,
   uF_tnrmDrastic,
   uF_tnrmEinstein,
   uF_tnrmHamacher
};

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_tnorm_dt uF_tnrmGet(int_fast8_t index)
{
   if (index >= uF_TNRM_NUM)
   {
      return 0;
   }
   return tnorms[index];
}

uF_fixed_point_dt uF_tnrmMin(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   return uF_fixpMin(a, b);
}

uF_fixed_point_dt uF_tnrmAlgebraic(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   return (uF_fixed_point_dt) uF_fixpMul(a, b);
}

uF_fixed_point_dt uF_tnrmBounded(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   return (a + b <= uF_FIXP_UNIT) ? 0 : a + b - uF_FIXP_UNIT;
}

uF_fixed_point_dt uF_tnrmDrastic(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   if (b == uF_FIXP_UNIT)
   {
      return a;
   } else
   {
      if (a == uF_FIXP_UNIT)
      {
         return b;
      }
      else
      {
         return 0;
      }
   }
}

uF_fixed_point_dt uF_tnrmEinstein(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt prod = uF_fixpMul(a, b);
   uF_double_fixed_point_dt sum = (uF_double_fixed_point_dt) a + b;
   return (uF_fixed_point_dt) uF_fixpDiv(prod, uF_fixpMake(2) - (sum - prod));
}

uF_fixed_point_dt uF_tnrmHamacher(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt prod = uF_fixpMul(a, b);
   uF_double_fixed_point_dt sum = (uF_double_fixed_point_dt) a + b;
   if (sum != prod)
   {
      return  (uF_fixed_point_dt) uF_fixpDiv(prod, (sum - prod));
   }
   return 0;
}

/*==================[end of file]============================================*/
