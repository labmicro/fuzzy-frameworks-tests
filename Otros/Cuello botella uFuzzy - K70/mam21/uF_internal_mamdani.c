/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_mamdani.h"

#include "uF_internal_defuzzify.h"
#include "uF_internal_output_variable.h"
#include "uF_internal_ruleset.h"
#include "uF_internal_utilities.h"

#include "uF_config.h"
#include "uF_fixed_point.h"
#include "uF_ufuzzy.h"

 #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** \brief Calculates the the triggering strength for all the membership
 ** functions of the given output variable for the current input state.
 **
 ** \param controller Pointer to the controller being used .
 ** \param output_var Output variable to process.
 ** \param[out] mf_strenght Array with the strength for each membership function.
 **/
static void GetTriggeringStrengths(uF_controller_dt *controller, uF_output_variable_dt *output_var)
{
	uint32_t aux = KIN1_GetCycleCounter();
	extern uint32_t uno;
   uF_fixed_point_dt (*connective)(uF_fixed_point_dt, uF_fixed_point_dt);
   uF_ruleset_dt *ruleset;
   uF_fixed_point_dt *input_weights;
   uint8_t *connective_type;
   int8_t *mem_func_id;
   uint_fast16_t *input_offset;
   uF_fixed_point_dt *mf_strengths;
   uF_fixed_point_dt weight;
   uint_fast8_t input_num;
   uint_fast8_t i, j;
   uint_fast8_t out_mf;
   uint_fast8_t out_mf_num;

   ruleset = (uF_ruleset_dt *) (&output_var->ruleset);
   input_weights = controller->input_weights;
   mf_strengths = output_var->mem_func_strengths;
   input_num = uF_ufuzInputNum(controller);
   out_mf_num = uF_ovarGetMemFuncNum(output_var);

   /* Clear the output array. */
   for (i = 0; i < out_mf_num; ++i)
   {
      mf_strengths[i] = 0;
   }

   /* For each rule. */
   for (i = 0; i < ruleset->rule_num; ++i)
   {
      /* Point to the logical connective for this rule. */
      connective_type = ruleset->data + i * ruleset->rule_size;

      /* Point to the membership function of the first input variable. */
      mem_func_id = (int8_t *) (connective_type + 1);

      /* Assign the input_offset to a local variable to improve performance. */
      input_offset = controller->input_offsets;

      /* Switch the connective type. */
      switch (*connective_type)
      {
         case uF_RSET_CONNECTIVE_AND:
            connective = controller->and_op;
            /* Assign the maximum value to the rule weight. */
            weight = uF_FIXP_UNIT;
            break;
         default:
            connective = controller->or_op;
            /* Assign the minimum value to the rule weight. */
            weight = 0;
      }

      /* For each input variable. */
      for (j = 0; j < input_num; ++j, ++mem_func_id, ++input_offset)
      {
         /* If the input variable appears in the rule. */
         if (*mem_func_id != 0)
         {
            /* Then accumulate its weight. */

            /* If the value is positive. */
            if(*mem_func_id > 0)
            {
               /* Then use its weight directly. */
               weight = connective(weight, input_weights[*mem_func_id - 1 + *input_offset]);
            }
            else
            {
               /* Else use the not operator. */
               weight = connective(weight, uF_FIXP_UNIT - input_weights[-*mem_func_id - 1 + *input_offset]);
            }
         }
         /* Else the input variable does not appear in the rule, i.e. must
          * be ignored for this calculation. */
      }


      /* The output membership function is the last one. */
      out_mf = *(mem_func_id);

      /* Accumulate the calculated weight to its position in the array. */
      mf_strengths[out_mf] = controller->or_op(mf_strengths[out_mf], weight);
   }
   uno = uno + KIN1_GetCycleCounter() - aux;
   return;
}

/*==================[external functions definition]==========================*/

uF_error_dt uF_mamdRead(uF_controller_dt *controller, uF_output_variable_dt *output_var, uF_reader_dt *reader)
{
   uF_ruleset_dt *ruleset;
   uint_fast16_t size;
   uint_fast8_t input;
   uint8_t rule_num;
   uint8_t rule;
   uint8_t tmp;
   int8_t mf_id;

   uF_utilAssert(output_var != 0);
   uF_utilAssert(controller != 0);
   uF_utilAssert(reader != 0);

   ruleset = &output_var->ruleset;

   /* Initialize the fields. */
   if (uF_rderRead8u(reader, &rule_num) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   ruleset->rule_num = rule_num;
   ruleset->data = uF_rderGetCursor(reader);

   /* Calculate the size of the rule antecedent. 1 byte for each input variable
    * plus one for the logical connective. */
   ruleset->antecedent_size = uF_ufuzInputNum(controller) + 1;

   /* Calculate the size of each rule in bytes. 1 byte for the consequent. */
   ruleset->rule_size = ruleset->antecedent_size + 1;

   for (rule = 0; rule < rule_num; ++rule)
   {
      /* Read the logic connective. */
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      if (tmp >= uF_RSET_CONNECTIVE_NUM)
      {
         return uF_ERROR_UNKNOWN_LOGICAL_CONNECTIVE;
      }
      /* Read the membership function for each input variable. */
      for (input = 0; input < uF_ufuzInputNum(controller); ++input)
      {
         if (uF_rderRead8(reader, &mf_id) != uF_ERROR_NONE)
         {
            return uF_ERROR_READER_OVERFLOW;
         }

         if (uF_utilAbs(mf_id) > uF_ivarGetMemFuncNum(&controller->inputs[input]))
         {
            return uF_ERROR_INVALID_MEMBERSHIP_FUNCTION_ID;
         }
      }

      /* Read the membership function of the output variable. */
      if (uF_rderRead8u(reader, (uint8_t *) &mf_id) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      if (mf_id >= uF_ovarGetMemFuncNum(output_var))
      {
         return uF_ERROR_INVALID_MEMBERSHIP_FUNCTION_ID;
      }
   }

   /* Add padding if necessary to align to 16 bits. */
   size = ruleset->rule_size * (uint_fast16_t) ruleset->rule_num;
   if ((size & 0x01u) != 0)
   {
      if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      else if (tmp != 0)
      {
         return uF_ERROR_PADDING_NOT_ZERO;
      }
   }
   return uF_ERROR_NONE;
}

uF_fixed_point_dt uF_mamdProcess(uF_controller_dt *controller, uF_output_variable_dt *output_var,
      uF_fixed_point_dt *is)
{
   /* Obtain the triggering strength for the output membership functions. */
   GetTriggeringStrengths(controller, output_var);

   switch (output_var->defuzzification)
   {
      default:
         return uF_dfuzCoG(output_var);
   }
}

/*==================[end of file]============================================*/
