/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "uF_internal_snorm.h"

#include "uF_fixed_point.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static uF_snorm_dt snorms[] =
{
   uF_snrmMax,
   uF_snrmAlgebraic,
   uF_snrmBounded,
   uF_snrmDrastic,
   uF_snrmEinstein,
   uF_snrmHamacher
};

/*==================[external data definition]===============================*/

/*=================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_snorm_dt uF_snrmGet(uint_fast8_t index)
{
   if (index >= uF_SNRM_NUM)
   {
      return 0;
   }
   return snorms[index];
}

uF_fixed_point_dt uF_snrmMax(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   return uF_fixpMax(a, b);
}

uF_fixed_point_dt uF_snrmAlgebraic(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt sum = (uF_double_fixed_point_dt) a + b;
   uF_double_fixed_point_dt prod = uF_fixpMul(a, b);
   return (uF_fixed_point_dt) (sum - prod);
}

uF_fixed_point_dt uF_snrmBounded(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt sum = (uF_double_fixed_point_dt) a + b;
   return (uF_fixed_point_dt) uF_fixpMin(sum,  uF_FIXP_UNIT);
}

uF_fixed_point_dt uF_snrmDrastic(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   return (uF_fixpMin(a, b)) ? uF_FIXP_UNIT : uF_fixpMax(a, b);
}

uF_fixed_point_dt uF_snrmEinstein(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt den = (uF_FIXP_UNIT + (uF_double_fixed_point_dt) uF_fixpMul(a, b));
   uF_double_fixed_point_dt num = (uF_double_fixed_point_dt) a + b;
   return (uF_fixed_point_dt) uF_fixpDiv(num, den);
}

uF_fixed_point_dt uF_snrmHamacher(uF_fixed_point_dt a, uF_fixed_point_dt b)
{
   uF_double_fixed_point_dt prod = uF_fixpMul(a, b);
   uF_double_fixed_point_dt den = (uF_FIXP_UNIT - prod);
   uF_double_fixed_point_dt num = (uF_double_fixed_point_dt) a + b - uF_fixpM2n(prod, 1);
   if (den != 0)
   {
      return (uF_fixed_point_dt) uF_fixpDiv(num, den);
   }
   return uF_FIXP_UNIT;
}

/*==================[end of file]============================================*/
