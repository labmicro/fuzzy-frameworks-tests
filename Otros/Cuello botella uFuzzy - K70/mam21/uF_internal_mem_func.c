/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_mem_func.h"
#include "uF_internal_reader.h"
#include "uF_internal_utilities.h"

#include "uF_error_code.h"
#include "uF_fixed_point.h"
#include "uF_memory_pool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** \brief Reads the data of a tabulated membership function.
 **
 ** \param mf Pointer to the membership function.
 ** \param reader Reader pointing to the beginning of the membership function
 ** data.
 ** \return uF_ERROR_NONE on success. The error code on error. */
static uF_error_dt ReadTable(uF_mem_func_dt *mf, uF_reader_dt *reader)
{
   uF_fixed_point_dt degree_of_membership;
   int_fast16_t i;
   int_fast16_t end;
   int16_t tmp;

   uF_utilAssert(mf != 0);
   uF_utilAssert(reader != 0);

   /* Read the table index size. */
   if (uF_rderRead8u(reader, &mf->data.tabulated.index_size) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   if (mf->data.tabulated.index_size > uF_MFUN_TABLE_MAX_INDEX_SIZE)
   {
      return uF_ERROR_TABLE_INDEX_SIZE_TOO_BIG;
   }

   mf->eval = uF_mfunEvalTable;
   mf->data.tabulated.point_y = (uint16_t *) uF_rderGetCursor(reader);

   end = 1 << mf->data.tabulated.index_size;
   for (i = 0; i < end; ++i)
   {
      if (uF_rderRead16(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      degree_of_membership = (uF_fixed_point_dt) uF_utilNtohs(tmp);
      if (degree_of_membership < 0 || degree_of_membership > uF_FIXP_UNIT)
      {
         return uF_ERROR_INVALID_TABLE_PARAMETERS;
      }
   }
   return uF_ERROR_NONE;
}

/** \brief Reads the data of a trapezoidal membership function.
 **
 ** \param mf Pointer to the membership function.
 ** \param reader Reader pointing to the beginning of the membership function
 ** data.
 ** \return uF_ERROR_NONE on success. The error code on error. */
static uF_error_dt ReadTrapezoid(uF_mem_func_dt *mf, uF_reader_dt *reader)
{
   uF_mem_func_data_dt *data;
   int16_t tmp;
   int_fast8_t i;
   uint8_t res;

   uF_utilAssert(mf != 0);
   uF_utilAssert(reader != 0);

   /* Read the reserved field. */
   if (uF_rderRead8u(reader, &res) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }

   data = &mf->data;
   mf->eval = uF_mfunEvalTrapezoid;

   for (i = 0; i < 4; ++i)
   {
      if (uF_rderRead16(reader, &tmp) != uF_ERROR_NONE)
      {
         return uF_ERROR_READER_OVERFLOW;
      }
      data->trapezoidal.point_x[i] = (uF_fixed_point_dt) uF_utilNtohs(tmp);
      if (data->trapezoidal.point_x[i] < 0 || data->trapezoidal.point_x[i] >= uF_FIXP_UNIT)
      {
         return uF_ERROR_INVALID_TRAPEZOID_PARAMETERS;
      }
   }
   return uF_ERROR_NONE;
}

/*==================[external functions definition]==========================*/

uF_error_dt uF_mfunRead(uF_mem_func_dt *mf, uF_reader_dt *reader)
{
   uint8_t type;

   uF_utilAssert(mf != 0);
   uF_utilAssert(reader != 0);

   /* Read the type. */
   if (uF_rderRead8u(reader, &type) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }

   if (uF_MFUN_TYPE_TRAPEZOID == type)
   {
      return ReadTrapezoid(mf, reader);
   }
   else if (uF_MFUN_TYPE_TABULATED == type)
   {
      return ReadTable(mf, reader);
   }
   else
   {
      return uF_ERROR_UNKNOWN_MEM_FUNC_TYPE;
   }
}

uF_fixed_point_dt uF_mfunEval(uF_mem_func_dt *mf, uF_fixed_point_dt x)
{
   return mf->eval(mf, x);
}

uF_fixed_point_dt uF_mfunEvalTable(uF_mem_func_dt *mf, uF_fixed_point_dt x)
{
   int_fast16_t index;
   int_fast8_t shift;

   uF_utilAssert(x >= 0);
   uF_utilAssert(x < uF_FIXP_UNIT);

   /* Calculate the number of bits that x must be shifted to be used as an
    * index of the table. */
   shift = uF_FIXP_FRACTIONAL_BITS - mf->data.tabulated.index_size;

   /* Calculate the index. */
   index = uF_fixpD2n(x, shift);
   return (uF_fixed_point_dt) uF_utilNtohs(mf->data.tabulated.point_y[index]);
}

uF_fixed_point_dt uF_mfunEvalTrapezoid(uF_mem_func_dt *mf, uF_fixed_point_dt x)
{
   uF_fixed_point_dt *point_x;
   uF_double_fixed_point_dt x1;
   uF_double_fixed_point_dt x2;

   point_x = mf->data.trapezoidal.point_x;

   if (x < point_x[0] || x > point_x[3])
   {
      return 0;
   }
   if (x > point_x[1] && x < point_x[2])
   {
      return uF_FIXP_UNIT;
   }

   x1 = point_x[0];
   x2 = point_x[1];
   if (x > point_x[1])
   {
      x1 = point_x[3];
      x2 = point_x[2];
   }

   /* If both are different. */
   if (x1 != x2)
   {
      /* The division can be performed. */
      return (uF_fixed_point_dt) uF_fixpDiv(((x - x1) << 1) + 1, (x2 - x1) << 1);
   }
   /* Else the slope is infinite. */

   /* If the points belongs to [1, 254] then return 0.5 else return 1. */
   return (x1 == 0 || x1 == uF_FIXP_UNIT - 1) ? uF_FIXP_UNIT : uF_fixpD2n(uF_FIXP_UNIT, 1);
}
/*==================[end of file]============================================*/
