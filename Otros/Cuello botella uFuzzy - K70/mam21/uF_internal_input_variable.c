/* Copyright 2016, Franco Javier Salinas Mendoza
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

#include "uF_internal_input_variable.h"

#include "uF_internal_mem_func.h"
#include "uF_internal_reader.h"
#include "uF_internal_utilities.h"

#include "uF_ufuzzy.h"
#include "uF_error_code.h"
#include "uF_memory_pool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_error_dt uF_ivarRead(uF_input_variable_dt *input_var, uF_reader_dt *reader)
{
   uF_error_dt error;
   int_fast8_t i;
   uint8_t mf_num;
   uint8_t tmp;

   uF_utilAssert(input_var != 0);
   uF_utilAssert(reader != 0);

   /* Read the number of membership functions. */
   if (uF_rderRead8u(reader, &mf_num) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }
   input_var->mf = uF_poolAlloc(mf_num * sizeof(uF_mem_func_dt));
   if (input_var->mf == 0)
   {
      return uF_ERROR_MEMORY_POOL_TOO_SMALL;
   }
   input_var->mem_func_num = mf_num;

   /* Read the reserved field. */
   if (uF_rderRead8u(reader, &tmp) != uF_ERROR_NONE)
   {
      return uF_ERROR_READER_OVERFLOW;
   }

   /* For each membership function. */
   for (i = 0; i < mf_num; ++i)
   {
      /* Read it. */
      error = uF_mfunRead(&input_var->mf[i], reader);
      if (error != uF_ERROR_NONE)
      {
         return error;
      }
   }

   return uF_ERROR_NONE;
}

uint_fast8_t uF_ivarGetMemFuncNum(uF_input_variable_dt *input_var)
{
   uF_utilAssert(input_var != 0);
   return input_var->mem_func_num;
}

/*==================[end of file]============================================*/
