#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
	Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* service = new FuzzyInput(1);
    FuzzySet* poor = new FuzzySet(0, 0, 1, 4);
    service->addFuzzySet(poor);
    FuzzySet* good = new FuzzySet(1, 4, 6, 9);
    service->addFuzzySet(good);
    FuzzySet* excellent = new FuzzySet(6, 9, 10, 10);
    service->addFuzzySet(excellent);
    fuzzy->addFuzzyInput(service);

    // FuzzyInput
    FuzzyInput* food = new FuzzyInput(2);
    FuzzySet* rancid = new FuzzySet(0, 0, 1, 3);
    food->addFuzzySet(rancid);
    FuzzySet* delicious = new FuzzySet(7, 9, 9, 10);
    food->addFuzzySet(delicious);
    fuzzy->addFuzzyInput(food);

	// FuzzyOutput
    FuzzyOutput* tip = new FuzzyOutput(1);
    FuzzySet* cheap = new FuzzySet(0, 5, 5, 10);
    tip->addFuzzySet(cheap);
    FuzzySet* average = new FuzzySet(10, 15, 15, 20);
    tip->addFuzzySet(average);
    FuzzySet* generous = new FuzzySet(20, 25, 25, 30);
    tip->addFuzzySet(generous);
    fuzzy->addFuzzyOutput(tip);

    // Regla1
    FuzzyRuleAntecedent* ifServicePoorORFoodRancid = new FuzzyRuleAntecedent();
    ifServicePoorORFoodRancid->joinWithOR(poor, rancid);

    FuzzyRuleConsequent* thenTipCheap = new FuzzyRuleConsequent();
    thenTipCheap->addOutput(cheap);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifServicePoorORFoodRancid, thenTipCheap);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifServiceGood = new FuzzyRuleAntecedent();
    ifServiceGood->joinSingle(good);

    FuzzyRuleConsequent* thenTipAverage = new FuzzyRuleConsequent();
    thenTipAverage->addOutput(average);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifServiceGood, thenTipAverage);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifServiceExellentOrFoodDelicious = new FuzzyRuleAntecedent();
    ifServiceExellentOrFoodDelicious->joinWithOR(excellent, delicious);

    FuzzyRuleConsequent* thenTipGenerous = new FuzzyRuleConsequent();
    thenTipGenerous->addOutput(generous);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifServiceExellentOrFoodDelicious, thenTipGenerous);
    fuzzy->addFuzzyRule(fuzzyRule3);

    for (int i = 0; i <= ciclos; ++i){
		fuzzy->setInput(1, 0 + (i * 10 / ciclos));
  		fuzzy->setInput(2, 0 + (i * 10 / ciclos));
  		fuzzy->fuzzify();
 		fuzzy->defuzzify(1);
	}
    while(1){};

    return 0;
}
