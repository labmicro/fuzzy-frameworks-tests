#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {

	Fuzzy* fuzzy = new Fuzzy();

	    // FuzzyInput
	    FuzzyInput* level = new FuzzyInput(1);
	    FuzzySet* high = new FuzzySet(-1, -1, -0.8, 0);
	    level->addFuzzySet(high);
	    FuzzySet* okay = new FuzzySet(-0.2, 0, 0, 0.5);
	    level->addFuzzySet(okay);
	    FuzzySet* low = new FuzzySet(0, 0.8, 1 ,1);
	    level->addFuzzySet(low);
	    fuzzy->addFuzzyInput(level);

	    // FuzzyInput
	    FuzzyInput* change = new FuzzyInput(2);
	    FuzzySet* falling = new FuzzySet(-0.1, -0.1, -0.05, 0);
	    change->addFuzzySet(falling);
	    FuzzySet* rising = new FuzzySet(0, 0.05, 0.1, 0.1);
	    change->addFuzzySet(rising);
	    fuzzy->addFuzzyInput(change);

		// FuzzyOutput
	    FuzzyOutput* valve = new FuzzyOutput(1);
	    FuzzySet* closeFast = new FuzzySet(-1, -0.9, -0.9, -0.8);
	    valve->addFuzzySet(closeFast);
	    FuzzySet* closeSlow = new FuzzySet(-0.6, -0.5, -0.5, -0.4);
	    valve->addFuzzySet(closeSlow);
	    FuzzySet* noChange = new FuzzySet(-0.1, 0, 0, 0.1);
	    valve->addFuzzySet(noChange);
		FuzzySet* openSlow = new FuzzySet(0.4, 0.5, 0.5, 0.6);
	    valve->addFuzzySet(openSlow);
	    FuzzySet* openFast = new FuzzySet(0.8, 0.9, 0.9, 1);
	    valve->addFuzzySet(openFast);
	    fuzzy->addFuzzyOutput(valve);

	    // Regla1
	    FuzzyRuleAntecedent* ifLevelLow = new FuzzyRuleAntecedent();
	    ifLevelLow->joinSingle(low);

	    FuzzyRuleConsequent* thenValveOpenFast = new FuzzyRuleConsequent();
	    thenValveOpenFast->addOutput(openFast);

	    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifLevelLow, thenValveOpenFast);
	    fuzzy->addFuzzyRule(fuzzyRule1);

	    // Regla2
	    FuzzyRuleAntecedent* ifLevelHigh = new FuzzyRuleAntecedent();
	    ifLevelHigh->joinSingle(high);

	    FuzzyRuleConsequent* thenValveCloseFast = new FuzzyRuleConsequent();
	    thenValveCloseFast->addOutput(closeFast);

	    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifLevelHigh, thenValveCloseFast);
	    fuzzy->addFuzzyRule(fuzzyRule2);

	    // Regla3
	    FuzzyRuleAntecedent* ifLevelOkayAndChangeRising = new FuzzyRuleAntecedent();
	    ifLevelOkayAndChangeRising->joinWithAND(okay, rising);

	    FuzzyRuleConsequent* thenValveCloseSlow = new FuzzyRuleConsequent();
	    thenValveCloseSlow->addOutput(closeSlow);

	    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifLevelOkayAndChangeRising, thenValveCloseSlow);
	    fuzzy->addFuzzyRule(fuzzyRule3);

	    // Regla4
	    FuzzyRuleAntecedent* ifLevelOkayAndChangeFalling = new FuzzyRuleAntecedent();
	    ifLevelOkayAndChangeFalling->joinWithAND(okay, falling);

	    FuzzyRuleConsequent* thenValveOpenSlow = new FuzzyRuleConsequent();
	    thenValveOpenSlow->addOutput(openSlow);

	    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifLevelOkayAndChangeFalling, thenValveOpenSlow);
	    fuzzy->addFuzzyRule(fuzzyRule4);

    for (int i = 0; i <= ciclos; ++i){
            fuzzy->setInput(1, -1 + (i * 1 / ciclos));
            fuzzy->setInput(2, -0.1 + (i * 0.1 / ciclos));
            fuzzy->fuzzify();
            fuzzy->defuzzify(1);
    }
    while(1){};

    return 0;
}
