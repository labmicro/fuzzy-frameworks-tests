#include <fl/Headers.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
int main(int argc, char *argv[]){
	std::string directorio = argv[1];
	std::string salida;
	//Import from FIS
	fl::Engine* engine = fl::FisImporter().fromFile(directorio);
	//Export to C++ file
	salida = fl::CppExporter().toString(engine);
	std::ofstream out("Salida.cpp");
	out << salida;
	out.close();
	return 0;
}
