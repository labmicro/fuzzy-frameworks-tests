/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>

#define ciclos 1000

int main(int argc, char** argv){
	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("tipper");
		engine->setDescription("");

		InputVariable* service = new InputVariable;
		service->setName("service");
		service->setDescription("");
		service->setEnabled(true);
		service->setRange(0.000, 10.000);
		service->setLockValueInRange(false);
		service->addTerm(new Gaussian("poor", 0.000, 1.500));
		service->addTerm(new Gaussian("good", 5.000, 1.500));
		service->addTerm(new Gaussian("excellent", 10.000, 1.500));
		engine->addInputVariable(service);

		InputVariable* food = new InputVariable;
		food->setName("food");
		food->setDescription("");
		food->setEnabled(true);
		food->setRange(0.000, 10.000);
		food->setLockValueInRange(false);
		food->addTerm(new Trapezoid("rancid", 0.000, 0.000, 1.000, 3.000));
		food->addTerm(new Trapezoid("delicious", 7.000, 9.000, 10.000, 10.000));
		engine->addInputVariable(food);

		OutputVariable* tip = new OutputVariable;
		tip->setName("tip");
		tip->setDescription("");
		tip->setEnabled(true);
		tip->setRange(0.000, 30.000);
		tip->setLockValueInRange(false);
		tip->setAggregation(new Maximum);
		tip->setDefuzzifier(new Centroid(100));
		tip->setDefaultValue(fl::nan);
		tip->setLockPreviousValue(false);
		tip->addTerm(new Triangle("cheap", 0.000, 5.000, 10.000));
		tip->addTerm(new Triangle("average", 10.000, 15.000, 20.000));
		tip->addTerm(new Triangle("generous", 20.000, 25.000, 30.000));
		engine->addOutputVariable(tip);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new Minimum);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new Minimum);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if service is poor or food is rancid then tip is cheap", engine));
		ruleBlock->addRule(Rule::parse("if service is good then tip is average", engine));
		ruleBlock->addRule(Rule::parse("if service is excellent or food is delicious then tip is generous", engine));
		engine->addRuleBlock(ruleBlock);

	for (int i = 0; i < ciclos; ++i){
		service->setValue(0 + (i * 10 / ciclos));
		food->setValue(0 + (i * 10 / ciclos));
		engine->process();
		tip->getValue();
    }
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
