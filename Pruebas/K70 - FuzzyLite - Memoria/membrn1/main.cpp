/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>

#define ciclos 1000
int main(int argc, char** argv){
		using namespace fl;

		Engine* engine = new Engine;
		engine->setName("anfis");
		engine->setDescription("");

		InputVariable* in_n1 = new InputVariable;
		in_n1->setName("in_n1");
		in_n1->setDescription("");
		in_n1->setEnabled(true);
		in_n1->setRange(1.000, 31.000);
		in_n1->setLockValueInRange(false);
		in_n1->addTerm(new Bell("in1mf1", 2.253, 16.220, 5.050));
		in_n1->addTerm(new Bell("in1mf2", 31.260, 15.021, 1.843));
		engine->addInputVariable(in_n1);

		InputVariable* in_n2 = new InputVariable;
		in_n2->setName("in_n2");
		in_n2->setDescription("");
		in_n2->setEnabled(true);
		in_n2->setRange(1.000, 31.000);
		in_n2->setLockValueInRange(false);
		in_n2->addTerm(new Bell("in2mf1", 0.740, 15.021, 1.843));
		in_n2->addTerm(new Bell("in2mf2", 29.747, 16.220, 5.050));
		engine->addInputVariable(in_n2);

		OutputVariable* out1 = new OutputVariable;
		out1->setName("out1");
		out1->setDescription("");
		out1->setEnabled(true);
		out1->setRange(-0.334, 1.000);
		out1->setLockValueInRange(false);
		out1->setAggregation(new Maximum);
		out1->setDefuzzifier(new WeightedAverage("Automatic"));
		out1->setDefaultValue(fl::nan);
		out1->setLockPreviousValue(false);
		out1->addTerm(Linear::create("out1mf1", engine, 0.026, 0.071, -0.615));
		out1->addTerm(Linear::create("out1mf2", engine, -0.036, 0.036, -1.169));
		out1->addTerm(Linear::create("out1mf3", engine, -0.094, 0.094, 2.231));
		out1->addTerm(Linear::create("out1mf4", engine, -0.071, -0.026, 2.479));
		engine->addOutputVariable(out1);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if in_n1 is in1mf1 and in_n2 is in2mf1 then out1 is out1mf1", engine));
		ruleBlock->addRule(Rule::parse("if in_n1 is in1mf1 and in_n2 is in2mf2 then out1 is out1mf2", engine));
		ruleBlock->addRule(Rule::parse("if in_n1 is in1mf2 and in_n2 is in2mf1 then out1 is out1mf3", engine));
		ruleBlock->addRule(Rule::parse("if in_n1 is in1mf2 and in_n2 is in2mf2 then out1 is out1mf4", engine));
		engine->addRuleBlock(ruleBlock);
	for (int i = 0; i < ciclos; ++i){
		in_n1->setValue(1 + (i * 31 / ciclos));
		in_n2->setValue(1 + (i * 31 / ciclos));
		engine->process();
		out1->getValue();
    }
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
