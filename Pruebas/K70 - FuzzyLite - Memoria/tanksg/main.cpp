/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>

#define ciclos 1000

int main(int argc, char** argv){
	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("tanksg1");
		engine->setDescription("");

		InputVariable* level = new InputVariable;
		level->setName("level");
		level->setDescription("");
		level->setEnabled(true);
		level->setRange(-1.000, 1.000);
		level->setLockValueInRange(false);
		level->addTerm(new Gaussian("high", -1.000, 0.300));
		level->addTerm(new Gaussian("okay", 0.004, 0.300));
		level->addTerm(new Gaussian("low", 1.000, 0.300));
		engine->addInputVariable(level);

		InputVariable* rate = new InputVariable;
		rate->setName("rate");
		rate->setDescription("");
		rate->setEnabled(true);
		rate->setRange(-0.100, 0.100);
		rate->setLockValueInRange(false);
		rate->addTerm(new Gaussian("negative", -0.100, 0.030));
		rate->addTerm(new Gaussian("none", 0.000, 0.030));
		rate->addTerm(new Gaussian("positive", 0.100, 0.030));
		engine->addInputVariable(rate);

		OutputVariable* valve = new OutputVariable;
		valve->setName("valve");
		valve->setDescription("");
		valve->setEnabled(true);
		valve->setRange(-1.000, 1.000);
		valve->setLockValueInRange(false);
		valve->setAggregation(new Maximum);
		valve->setDefuzzifier(new WeightedAverage("Automatic"));
		valve->setDefaultValue(fl::nan);
		valve->setLockPreviousValue(false);
		valve->addTerm(new Constant("close_fast", -0.900));
		valve->addTerm(new Constant("close_slow", -0.500));
		valve->addTerm(new Constant("no_change", 0.000));
		valve->addTerm(new Constant("open_slow", 0.300));
		valve->addTerm(new Constant("open_fast", 0.900));
		engine->addOutputVariable(valve);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new AlgebraicSum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if level is okay then valve is no_change", engine));
		ruleBlock->addRule(Rule::parse("if level is low then valve is open_fast", engine));
		ruleBlock->addRule(Rule::parse("if level is high then valve is close_fast", engine));
		ruleBlock->addRule(Rule::parse("if level is okay and rate is positive then valve is close_slow", engine));
		ruleBlock->addRule(Rule::parse("if level is okay and rate is negative then valve is open_slow", engine));
		engine->addRuleBlock(ruleBlock);

	for (int i = 0; i < ciclos; ++i){
		level->setValue(-1 + (i * 1 / ciclos));
		rate->setValue(-0.1 + (i * 0.1 / ciclos));
		engine->process();
		valve->getValue();
    }
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
