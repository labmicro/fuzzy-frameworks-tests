/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>

#define ciclos 1000

int main(int argc, char** argv){
	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("tank");
		engine->setDescription("");

		InputVariable* level = new InputVariable;
		level->setName("level");
		level->setDescription("");
		level->setEnabled(true);
		level->setRange(-1.000, 1.000);
		level->setLockValueInRange(false);
		level->addTerm(new Trapezoid("high", -2.000, -1.000, -0.800, -0.001));
		level->addTerm(new Triangle("good", -0.150, 0.000, 0.500));
		level->addTerm(new Trapezoid("low", 0.001, 0.800, 1.000, 1.500));
		engine->addInputVariable(level);

		InputVariable* change = new InputVariable;
		change->setName("change");
		change->setDescription("");
		change->setEnabled(true);
		change->setRange(-0.100, 0.100);
		change->setLockValueInRange(false);
		change->addTerm(new Trapezoid("falling", -0.140, -0.100, -0.060, 0.000));
		change->addTerm(new Trapezoid("rising", -0.001, 0.060, 0.100, 0.140));
		engine->addInputVariable(change);

		OutputVariable* valve = new OutputVariable;
		valve->setName("valve");
		valve->setDescription("");
		valve->setEnabled(true);
		valve->setRange(-1.000, 1.000);
		valve->setLockValueInRange(false);
		valve->setAggregation(new Maximum);
		valve->setDefuzzifier(new Centroid(100));
		valve->setDefaultValue(fl::nan);
		valve->setLockPreviousValue(false);
		valve->addTerm(new Triangle("close_fast", -1.000, -0.900, -0.800));
		valve->addTerm(new Triangle("close_slow", -0.600, -0.500, -0.400));
		valve->addTerm(new Triangle("no_change", -0.100, 0.000, 0.100));
		valve->addTerm(new Triangle("open_slow", 0.400, 0.500, 0.600));
		valve->addTerm(new Triangle("open_fast", 0.800, 0.900, 1.000));
		engine->addOutputVariable(valve);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new AlgebraicSum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if level is low then valve is open_fast", engine));
		ruleBlock->addRule(Rule::parse("if level is high then valve is close_fast", engine));
		ruleBlock->addRule(Rule::parse("if level is good and change is rising then valve is close_slow", engine));
		ruleBlock->addRule(Rule::parse("if level is good and change is falling then valve is open_slow", engine));
		engine->addRuleBlock(ruleBlock);
		for (int i = 0; i < ciclos; ++i){
		level->setValue(-1 + (i * 1 / ciclos));
		change->setValue(-0.1 + (i * 0.1 / ciclos));
		engine->process();
		valve->getValue();
    }
		while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
