/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>

#define ciclos 1000

int main(int argc, char** argv){
		using namespace fl;

		Engine* engine = new Engine;
		engine->setName("sugeno");
		engine->setDescription("");

		InputVariable* input = new InputVariable;
		input->setName("input");
		input->setDescription("");
		input->setEnabled(true);
		input->setRange(-5.000, 5.000);
		input->setLockValueInRange(false);
		input->addTerm(new Gaussian("low", -5.000, 4.000));
		input->addTerm(new Gaussian("high", 5.000, 4.000));
		engine->addInputVariable(input);

		OutputVariable* output = new OutputVariable;
		output->setName("output");
		output->setDescription("");
		output->setEnabled(true);
		output->setRange(0.000, 1.000);
		output->setLockValueInRange(false);
		output->setAggregation(new Maximum);
		output->setDefuzzifier(new WeightedAverage("Automatic"));
		output->setDefaultValue(fl::nan);
		output->setLockPreviousValue(false);
		output->addTerm(Linear::create("line1", engine, -1.000, -1.000));
		output->addTerm(Linear::create("line2", engine, 1.000, -1.000));
		engine->addOutputVariable(output);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new AlgebraicSum);
		ruleBlock->setImplication(new Minimum);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if input is low then output is line1", engine));
		ruleBlock->addRule(Rule::parse("if input is high then output is line2", engine));
		engine->addRuleBlock(ruleBlock);

	for (int i = 0; i < ciclos; ++i){
		input->setValue(-5 + (i * 5 / ciclos));
		engine->process();
		output->getValue();
    }
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
