#include <fl/Headers.h>
#include <iostream>
#define ciclos 50000
int main(int argc, char** argv){
 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("sugeno");
	engine->setDescription("");

	InputVariable* input = new InputVariable;
	input->setName("input");
	input->setDescription("");
	input->setEnabled(true);
	input->setRange(-5.000, 5.000);
	input->setLockValueInRange(false);
	input->addTerm(new Gaussian("low", -5.000, 4.000));
	input->addTerm(new Gaussian("high", 5.000, 4.000));
	engine->addInputVariable(input);

	OutputVariable* output = new OutputVariable;
	output->setName("output");
	output->setDescription("");
	output->setEnabled(true);
	output->setRange(0.000, 1.000);
	output->setLockValueInRange(false);
	output->setAggregation(new Maximum);
	output->setDefuzzifier(new WeightedAverage("Automatic"));
	output->setDefaultValue(fl::nan);
	output->setLockPreviousValue(false);
	output->addTerm(Linear::create("line1", engine, -1.000, -1.000));
	output->addTerm(Linear::create("line2", engine, 1.000, -1.000));
	engine->addOutputVariable(output);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new AlgebraicProduct);
	ruleBlock->setDisjunction(new AlgebraicSum);
	ruleBlock->setImplication(new Minimum);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if input is low then output is line1", engine));
	ruleBlock->addRule(Rule::parse("if input is high then output is line2", engine));
	engine->addRuleBlock(ruleBlock);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		input->setValue(-5 + (i * 5 / ciclos));
		engine->process();
		output->getValue();
    }
return 0;
}
