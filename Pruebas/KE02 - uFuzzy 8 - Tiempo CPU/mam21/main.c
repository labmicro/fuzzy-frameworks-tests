#include <stdio.h>
#include <stdint.h>

#include "uF_ufuzzy.h"
#include "uF_fixed_point.h"
#include "MKE02Z2.h"
#define ciclos 10000
/* Estructura CFS donde se especifican las reglas. */
const uint8_t cfs_data[] = {
	0x43u, 0x46u, 0x53u, 0x00u, 0x00u, 0x00u, 0x02u, 0x01u, 0x02u, 0x00u, 0x01u,
0x08u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu, 0xffu,
0x3fu, 0xffu, 0x3fu, 0xfeu, 0x3fu, 0xfeu, 0x3fu, 0xfdu, 0x3fu, 0xfcu, 0x3fu,
0xfcu, 0x3fu, 0xfbu, 0x3fu, 0xf9u, 0x3fu, 0xf8u, 0x3fu, 0xf6u, 0x3fu, 0xf4u,
0x3fu, 0xf1u, 0x3fu, 0xeeu, 0x3fu, 0xebu, 0x3fu, 0xe6u, 0x3fu, 0xe1u, 0x3fu,
0xdbu, 0x3fu, 0xd3u, 0x3fu, 0xcbu, 0x3fu, 0xc0u, 0x3fu, 0xb4u, 0x3fu, 0xa6u,
0x3fu, 0x95u, 0x3fu, 0x81u, 0x3fu, 0x6au, 0x3fu, 0x4fu, 0x3fu, 0x30u, 0x3fu,
0x0cu, 0x3eu, 0xe2u, 0x3eu, 0xb1u, 0x3eu, 0x79u, 0x3eu, 0x39u, 0x3du, 0xefu,
0x3du, 0x9au, 0x3du, 0x39u, 0x3cu, 0xcbu, 0x3cu, 0x4eu, 0x3bu, 0xc1u, 0x3bu,
0x22u, 0x3au, 0x70u, 0x39u, 0xaau, 0x38u, 0xccu, 0x37u, 0xd8u, 0x36u, 0xcau,
0x35u, 0xa3u, 0x34u, 0x62u, 0x33u, 0x07u, 0x31u, 0x91u, 0x30u, 0x02u, 0x2eu,
0x5cu, 0x2cu, 0x9eu, 0x2au, 0xcdu, 0x28u, 0xebu, 0x26u, 0xfcu, 0x25u, 0x02u,
0x23u, 0x02u, 0x21u, 0x00u, 0x1fu, 0x01u, 0x1du, 0x07u, 0x1bu, 0x16u, 0x19u,
0x33u, 0x17u, 0x60u, 0x15u, 0x9eu, 0x13u, 0xf2u, 0x12u, 0x5bu, 0x10u, 0xdau,
0x0fu, 0x72u, 0x0eu, 0x20u, 0x0cu, 0xe6u, 0x0bu, 0xc3u, 0x0au, 0xb6u, 0x09u,
0xbfu, 0x08u, 0xdbu, 0x08u, 0x0bu, 0x07u, 0x4cu, 0x06u, 0x9eu, 0x06u, 0x00u,
0x05u, 0x70u, 0x04u, 0xedu, 0x04u, 0x76u, 0x04u, 0x0bu, 0x03u, 0xa9u, 0x03u,
0x51u, 0x03u, 0x01u, 0x02u, 0xb9u, 0x02u, 0x78u, 0x02u, 0x3du, 0x02u, 0x07u,
0x01u, 0xd7u, 0x01u, 0xabu, 0x01u, 0x84u, 0x01u, 0x60u, 0x01u, 0x40u, 0x01u,
0x23u, 0x01u, 0x08u, 0x00u, 0xf0u, 0x00u, 0xdbu, 0x00u, 0xc7u, 0x00u, 0xb5u,
0x00u, 0xa5u, 0x00u, 0x97u, 0x00u, 0x89u, 0x00u, 0x7du, 0x00u, 0x72u, 0x00u,
0x68u, 0x00u, 0x5fu, 0x00u, 0x57u, 0x00u, 0x50u, 0x00u, 0x49u, 0x00u, 0x43u,
0x00u, 0x3du, 0x00u, 0x38u, 0x00u, 0x33u, 0x00u, 0x2fu, 0x00u, 0x2bu, 0x00u,
0x28u, 0x00u, 0x24u, 0x00u, 0x21u, 0x00u, 0x1fu, 0x00u, 0x1cu, 0x00u, 0x1au,
0x00u, 0x18u, 0x00u, 0x16u, 0x00u, 0x14u, 0x00u, 0x13u, 0x00u, 0x11u, 0x00u,
0x10u, 0x00u, 0x0fu, 0x00u, 0x0eu, 0x00u, 0x0cu, 0x00u, 0x0cu, 0x00u, 0x0bu,
0x00u, 0x0au, 0x00u, 0x09u, 0x00u, 0x08u, 0x00u, 0x08u, 0x00u, 0x07u, 0x00u,
0x07u, 0x00u, 0x06u, 0x00u, 0x06u, 0x00u, 0x05u, 0x00u, 0x05u, 0x00u, 0x05u,
0x00u, 0x04u, 0x00u, 0x04u, 0x00u, 0x04u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u,
0x03u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u,
0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u,
0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x08u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u,
0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u,
0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x02u, 0x00u,
0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u,
0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u,
0x04u, 0x00u, 0x04u, 0x00u, 0x04u, 0x00u, 0x05u, 0x00u, 0x05u, 0x00u, 0x05u,
0x00u, 0x06u, 0x00u, 0x06u, 0x00u, 0x07u, 0x00u, 0x07u, 0x00u, 0x08u, 0x00u,
0x08u, 0x00u, 0x09u, 0x00u, 0x0au, 0x00u, 0x0bu, 0x00u, 0x0cu, 0x00u, 0x0cu,
0x00u, 0x0eu, 0x00u, 0x0fu, 0x00u, 0x10u, 0x00u, 0x11u, 0x00u, 0x13u, 0x00u,
0x14u, 0x00u, 0x16u, 0x00u, 0x18u, 0x00u, 0x1au, 0x00u, 0x1cu, 0x00u, 0x1fu,
0x00u, 0x21u, 0x00u, 0x24u, 0x00u, 0x28u, 0x00u, 0x2bu, 0x00u, 0x2fu, 0x00u,
0x33u, 0x00u, 0x38u, 0x00u, 0x3du, 0x00u, 0x43u, 0x00u, 0x49u, 0x00u, 0x50u,
0x00u, 0x57u, 0x00u, 0x5fu, 0x00u, 0x68u, 0x00u, 0x72u, 0x00u, 0x7du, 0x00u,
0x89u, 0x00u, 0x97u, 0x00u, 0xa5u, 0x00u, 0xb5u, 0x00u, 0xc7u, 0x00u, 0xdbu,
0x00u, 0xf0u, 0x01u, 0x08u, 0x01u, 0x23u, 0x01u, 0x40u, 0x01u, 0x60u, 0x01u,
0x84u, 0x01u, 0xabu, 0x01u, 0xd7u, 0x02u, 0x07u, 0x02u, 0x3du, 0x02u, 0x78u,
0x02u, 0xb9u, 0x03u, 0x01u, 0x03u, 0x51u, 0x03u, 0xa9u, 0x04u, 0x0bu, 0x04u,
0x76u, 0x04u, 0xedu, 0x05u, 0x70u, 0x06u, 0x00u, 0x06u, 0x9eu, 0x07u, 0x4cu,
0x08u, 0x0bu, 0x08u, 0xdbu, 0x09u, 0xbfu, 0x0au, 0xb6u, 0x0bu, 0xc3u, 0x0cu,
0xe6u, 0x0eu, 0x20u, 0x0fu, 0x72u, 0x10u, 0xdau, 0x12u, 0x5bu, 0x13u, 0xf2u,
0x15u, 0x9eu, 0x17u, 0x60u, 0x19u, 0x33u, 0x1bu, 0x16u, 0x1du, 0x07u, 0x1fu,
0x01u, 0x21u, 0x00u, 0x23u, 0x02u, 0x25u, 0x02u, 0x26u, 0xfcu, 0x28u, 0xebu,
0x2au, 0xcdu, 0x2cu, 0x9eu, 0x2eu, 0x5cu, 0x30u, 0x02u, 0x31u, 0x91u, 0x33u,
0x07u, 0x34u, 0x62u, 0x35u, 0xa3u, 0x36u, 0xcau, 0x37u, 0xd8u, 0x38u, 0xccu,
0x39u, 0xaau, 0x3au, 0x70u, 0x3bu, 0x22u, 0x3bu, 0xc1u, 0x3cu, 0x4eu, 0x3cu,
0xcbu, 0x3du, 0x39u, 0x3du, 0x9au, 0x3du, 0xefu, 0x3eu, 0x39u, 0x3eu, 0x79u,
0x3eu, 0xb1u, 0x3eu, 0xe2u, 0x3fu, 0x0cu, 0x3fu, 0x30u, 0x3fu, 0x4fu, 0x3fu,
0x6au, 0x3fu, 0x81u, 0x3fu, 0x95u, 0x3fu, 0xa6u, 0x3fu, 0xb4u, 0x3fu, 0xc0u,
0x3fu, 0xcbu, 0x3fu, 0xd3u, 0x3fu, 0xdbu, 0x3fu, 0xe1u, 0x3fu, 0xe6u, 0x3fu,
0xebu, 0x3fu, 0xeeu, 0x3fu, 0xf1u, 0x3fu, 0xf4u, 0x3fu, 0xf6u, 0x3fu, 0xf8u,
0x3fu, 0xf9u, 0x3fu, 0xfbu, 0x3fu, 0xfcu, 0x3fu, 0xfcu, 0x3fu, 0xfdu, 0x3fu,
0xfeu, 0x3fu, 0xfeu, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu, 0xffu,
0x3fu, 0xffu, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x02u, 0x00u, 0x01u, 0x08u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu,
0xffu, 0x3fu, 0xfeu, 0x3fu, 0xfdu, 0x3fu, 0xfcu, 0x3fu, 0xfbu, 0x3fu, 0xfau,
0x3fu, 0xf9u, 0x3fu, 0xf7u, 0x3fu, 0xf5u, 0x3fu, 0xf3u, 0x3fu, 0xf0u, 0x3fu,
0xedu, 0x3fu, 0xeau, 0x3fu, 0xe6u, 0x3fu, 0xe2u, 0x3fu, 0xddu, 0x3fu, 0xd8u,
0x3fu, 0xd2u, 0x3fu, 0xcbu, 0x3fu, 0xc4u, 0x3fu, 0xbcu, 0x3fu, 0xb3u, 0x3fu,
0xaau, 0x3fu, 0xa0u, 0x3fu, 0x94u, 0x3fu, 0x88u, 0x3fu, 0x7bu, 0x3fu, 0x6du,
0x3fu, 0x5du, 0x3fu, 0x4du, 0x3fu, 0x3bu, 0x3fu, 0x28u, 0x3fu, 0x14u, 0x3eu,
0xffu, 0x3eu, 0xe8u, 0x3eu, 0xcfu, 0x3eu, 0xb5u, 0x3eu, 0x9au, 0x3eu, 0x7cu,
0x3eu, 0x5eu, 0x3eu, 0x3du, 0x3eu, 0x1bu, 0x3du, 0xf7u, 0x3du, 0xd1u, 0x3du,
0xa9u, 0x3du, 0x7fu, 0x3du, 0x53u, 0x3du, 0x25u, 0x3cu, 0xf5u, 0x3cu, 0xc3u,
0x3cu, 0x8fu, 0x3cu, 0x58u, 0x3cu, 0x20u, 0x3bu, 0xe5u, 0x3bu, 0xa7u, 0x3bu,
0x68u, 0x3bu, 0x26u, 0x3au, 0xe2u, 0x3au, 0x9bu, 0x3au, 0x52u, 0x3au, 0x07u,
0x39u, 0xb9u, 0x39u, 0x69u, 0x39u, 0x17u, 0x38u, 0xc2u, 0x38u, 0x6bu, 0x38u,
0x12u, 0x37u, 0xb6u, 0x37u, 0x58u, 0x36u, 0xf7u, 0x36u, 0x95u, 0x36u, 0x30u,
0x35u, 0xc9u, 0x35u, 0x60u, 0x34u, 0xf5u, 0x34u, 0x87u, 0x34u, 0x18u, 0x33u,
0xa7u, 0x33u, 0x34u, 0x32u, 0xc0u, 0x32u, 0x49u, 0x31u, 0xd1u, 0x31u, 0x57u,
0x30u, 0xdcu, 0x30u, 0x60u, 0x2fu, 0xe2u, 0x2fu, 0x62u, 0x2eu, 0xe2u, 0x2eu,
0x60u, 0x2du, 0xdeu, 0x2du, 0x5au, 0x2cu, 0xd6u, 0x2cu, 0x50u, 0x2bu, 0xcau,
0x2bu, 0x44u, 0x2au, 0xbdu, 0x2au, 0x35u, 0x29u, 0xaeu, 0x29u, 0x26u, 0x28u,
0x9du, 0x28u, 0x15u, 0x27u, 0x8du, 0x27u, 0x04u, 0x26u, 0x7cu, 0x25u, 0xf4u,
0x25u, 0x6du, 0x24u, 0xe6u, 0x24u, 0x5fu, 0x23u, 0xd8u, 0x23u, 0x53u, 0x22u,
0xceu, 0x22u, 0x49u, 0x21u, 0xc6u, 0x21u, 0x43u, 0x20u, 0xc1u, 0x20u, 0x40u,
0x1fu, 0xc0u, 0x1fu, 0x41u, 0x1eu, 0xc3u, 0x1eu, 0x46u, 0x1du, 0xcbu, 0x1du,
0x50u, 0x1cu, 0xd7u, 0x1cu, 0x5fu, 0x1bu, 0xe8u, 0x1bu, 0x73u, 0x1au, 0xffu,
0x1au, 0x8cu, 0x1au, 0x1bu, 0x19u, 0xabu, 0x19u, 0x3cu, 0x18u, 0xcfu, 0x18u,
0x63u, 0x17u, 0xf9u, 0x17u, 0x90u, 0x17u, 0x29u, 0x16u, 0xc3u, 0x16u, 0x5fu,
0x15u, 0xfcu, 0x15u, 0x9au, 0x15u, 0x3au, 0x14u, 0xdcu, 0x14u, 0x7fu, 0x14u,
0x23u, 0x13u, 0xc9u, 0x13u, 0x70u, 0x13u, 0x19u, 0x12u, 0xc3u, 0x12u, 0x6fu,
0x12u, 0x1cu, 0x11u, 0xcau, 0x11u, 0x7au, 0x11u, 0x2bu, 0x10u, 0xddu, 0x10u,
0x91u, 0x10u, 0x46u, 0x0fu, 0xfdu, 0x0fu, 0xb5u, 0x0fu, 0x6eu, 0x0fu, 0x28u,
0x0eu, 0xe4u, 0x0eu, 0xa0u, 0x0eu, 0x5eu, 0x0eu, 0x1eu, 0x0du, 0xdeu, 0x0du,
0xa0u, 0x0du, 0x62u, 0x0du, 0x26u, 0x0cu, 0xebu, 0x0cu, 0xb1u, 0x0cu, 0x78u,
0x0cu, 0x40u, 0x0cu, 0x0au, 0x0bu, 0xd4u, 0x0bu, 0x9fu, 0x0bu, 0x6bu, 0x0bu,
0x39u, 0x0bu, 0x07u, 0x0au, 0xd6u, 0x0au, 0xa6u, 0x0au, 0x77u, 0x0au, 0x49u,
0x0au, 0x1cu, 0x09u, 0xefu, 0x09u, 0xc4u, 0x09u, 0x99u, 0x09u, 0x6fu, 0x09u,
0x46u, 0x09u, 0x1eu, 0x08u, 0xf6u, 0x08u, 0xcfu, 0x08u, 0xa9u, 0x08u, 0x84u,
0x08u, 0x5fu, 0x08u, 0x3cu, 0x08u, 0x18u, 0x07u, 0xf6u, 0x07u, 0xd4u, 0x07u,
0xb3u, 0x07u, 0x92u, 0x07u, 0x72u, 0x07u, 0x53u, 0x07u, 0x34u, 0x07u, 0x16u,
0x06u, 0xf8u, 0x06u, 0xdbu, 0x06u, 0xbeu, 0x06u, 0xa2u, 0x06u, 0x87u, 0x06u,
0x6cu, 0x06u, 0x51u, 0x06u, 0x37u, 0x06u, 0x1eu, 0x06u, 0x05u, 0x05u, 0xecu,
0x05u, 0xd4u, 0x05u, 0xbdu, 0x05u, 0xa5u, 0x05u, 0x8fu, 0x05u, 0x78u, 0x05u,
0x62u, 0x05u, 0x4du, 0x05u, 0x38u, 0x05u, 0x23u, 0x05u, 0x0fu, 0x04u, 0xfbu,
0x04u, 0xe7u, 0x04u, 0xd4u, 0x04u, 0xc1u, 0x04u, 0xaeu, 0x04u, 0x9cu, 0x04u,
0x8au, 0x04u, 0x79u, 0x04u, 0x67u, 0x04u, 0x56u, 0x04u, 0x46u, 0x04u, 0x35u,
0x04u, 0x25u, 0x04u, 0x16u, 0x04u, 0x06u, 0x03u, 0xf7u, 0x03u, 0xe8u, 0x03u,
0xd9u, 0x03u, 0xcbu, 0x01u, 0x08u, 0x03u, 0xcbu, 0x03u, 0xd9u, 0x03u, 0xe8u,
0x03u, 0xf7u, 0x04u, 0x06u, 0x04u, 0x16u, 0x04u, 0x25u, 0x04u, 0x35u, 0x04u,
0x46u, 0x04u, 0x56u, 0x04u, 0x67u, 0x04u, 0x79u, 0x04u, 0x8au, 0x04u, 0x9cu,
0x04u, 0xaeu, 0x04u, 0xc1u, 0x04u, 0xd4u, 0x04u, 0xe7u, 0x04u, 0xfbu, 0x05u,
0x0fu, 0x05u, 0x23u, 0x05u, 0x38u, 0x05u, 0x4du, 0x05u, 0x62u, 0x05u, 0x78u,
0x05u, 0x8fu, 0x05u, 0xa5u, 0x05u, 0xbdu, 0x05u, 0xd4u, 0x05u, 0xecu, 0x06u,
0x05u, 0x06u, 0x1eu, 0x06u, 0x37u, 0x06u, 0x51u, 0x06u, 0x6cu, 0x06u, 0x87u,
0x06u, 0xa2u, 0x06u, 0xbeu, 0x06u, 0xdbu, 0x06u, 0xf8u, 0x07u, 0x16u, 0x07u,
0x34u, 0x07u, 0x53u, 0x07u, 0x72u, 0x07u, 0x92u, 0x07u, 0xb3u, 0x07u, 0xd4u,
0x07u, 0xf6u, 0x08u, 0x18u, 0x08u, 0x3cu, 0x08u, 0x5fu, 0x08u, 0x84u, 0x08u,
0xa9u, 0x08u, 0xcfu, 0x08u, 0xf6u, 0x09u, 0x1eu, 0x09u, 0x46u, 0x09u, 0x6fu,
0x09u, 0x99u, 0x09u, 0xc4u, 0x09u, 0xefu, 0x0au, 0x1cu, 0x0au, 0x49u, 0x0au,
0x77u, 0x0au, 0xa6u, 0x0au, 0xd6u, 0x0bu, 0x07u, 0x0bu, 0x39u, 0x0bu, 0x6bu,
0x0bu, 0x9fu, 0x0bu, 0xd4u, 0x0cu, 0x0au, 0x0cu, 0x40u, 0x0cu, 0x78u, 0x0cu,
0xb1u, 0x0cu, 0xebu, 0x0du, 0x26u, 0x0du, 0x62u, 0x0du, 0xa0u, 0x0du, 0xdeu,
0x0eu, 0x1eu, 0x0eu, 0x5eu, 0x0eu, 0xa0u, 0x0eu, 0xe4u, 0x0fu, 0x28u, 0x0fu,
0x6eu, 0x0fu, 0xb5u, 0x0fu, 0xfdu, 0x10u, 0x46u, 0x10u, 0x91u, 0x10u, 0xddu,
0x11u, 0x2bu, 0x11u, 0x7au, 0x11u, 0xcau, 0x12u, 0x1cu, 0x12u, 0x6fu, 0x12u,
0xc3u, 0x13u, 0x19u, 0x13u, 0x70u, 0x13u, 0xc9u, 0x14u, 0x23u, 0x14u, 0x7fu,
0x14u, 0xdcu, 0x15u, 0x3au, 0x15u, 0x9au, 0x15u, 0xfcu, 0x16u, 0x5fu, 0x16u,
0xc3u, 0x17u, 0x29u, 0x17u, 0x90u, 0x17u, 0xf9u, 0x18u, 0x63u, 0x18u, 0xcfu,
0x19u, 0x3cu, 0x19u, 0xabu, 0x1au, 0x1bu, 0x1au, 0x8cu, 0x1au, 0xffu, 0x1bu,
0x73u, 0x1bu, 0xe8u, 0x1cu, 0x5fu, 0x1cu, 0xd7u, 0x1du, 0x50u, 0x1du, 0xcbu,
0x1eu, 0x46u, 0x1eu, 0xc3u, 0x1fu, 0x41u, 0x1fu, 0xc0u, 0x20u, 0x40u, 0x20u,
0xc1u, 0x21u, 0x43u, 0x21u, 0xc6u, 0x22u, 0x49u, 0x22u, 0xceu, 0x23u, 0x53u,
0x23u, 0xd8u, 0x24u, 0x5fu, 0x24u, 0xe6u, 0x25u, 0x6du, 0x25u, 0xf4u, 0x26u,
0x7cu, 0x27u, 0x04u, 0x27u, 0x8du, 0x28u, 0x15u, 0x28u, 0x9du, 0x29u, 0x26u,
0x29u, 0xaeu, 0x2au, 0x35u, 0x2au, 0xbdu, 0x2bu, 0x44u, 0x2bu, 0xcau, 0x2cu,
0x50u, 0x2cu, 0xd6u, 0x2du, 0x5au, 0x2du, 0xdeu, 0x2eu, 0x60u, 0x2eu, 0xe2u,
0x2fu, 0x62u, 0x2fu, 0xe2u, 0x30u, 0x60u, 0x30u, 0xdcu, 0x31u, 0x57u, 0x31u,
0xd1u, 0x32u, 0x49u, 0x32u, 0xc0u, 0x33u, 0x34u, 0x33u, 0xa7u, 0x34u, 0x18u,
0x34u, 0x87u, 0x34u, 0xf5u, 0x35u, 0x60u, 0x35u, 0xc9u, 0x36u, 0x30u, 0x36u,
0x95u, 0x36u, 0xf7u, 0x37u, 0x58u, 0x37u, 0xb6u, 0x38u, 0x12u, 0x38u, 0x6bu,
0x38u, 0xc2u, 0x39u, 0x17u, 0x39u, 0x69u, 0x39u, 0xb9u, 0x3au, 0x07u, 0x3au,
0x52u, 0x3au, 0x9bu, 0x3au, 0xe2u, 0x3bu, 0x26u, 0x3bu, 0x68u, 0x3bu, 0xa7u,
0x3bu, 0xe5u, 0x3cu, 0x20u, 0x3cu, 0x58u, 0x3cu, 0x8fu, 0x3cu, 0xc3u, 0x3cu,
0xf5u, 0x3du, 0x25u, 0x3du, 0x53u, 0x3du, 0x7fu, 0x3du, 0xa9u, 0x3du, 0xd1u,
0x3du, 0xf7u, 0x3eu, 0x1bu, 0x3eu, 0x3du, 0x3eu, 0x5eu, 0x3eu, 0x7cu, 0x3eu,
0x9au, 0x3eu, 0xb5u, 0x3eu, 0xcfu, 0x3eu, 0xe8u, 0x3eu, 0xffu, 0x3fu, 0x14u,
0x3fu, 0x28u, 0x3fu, 0x3bu, 0x3fu, 0x4du, 0x3fu, 0x5du, 0x3fu, 0x6du, 0x3fu,
0x7bu, 0x3fu, 0x88u, 0x3fu, 0x94u, 0x3fu, 0xa0u, 0x3fu, 0xaau, 0x3fu, 0xb3u,
0x3fu, 0xbcu, 0x3fu, 0xc4u, 0x3fu, 0xcbu, 0x3fu, 0xd2u, 0x3fu, 0xd8u, 0x3fu,
0xddu, 0x3fu, 0xe2u, 0x3fu, 0xe6u, 0x3fu, 0xeau, 0x3fu, 0xedu, 0x3fu, 0xf0u,
0x3fu, 0xf3u, 0x3fu, 0xf5u, 0x3fu, 0xf7u, 0x3fu, 0xf9u, 0x3fu, 0xfau, 0x3fu,
0xfbu, 0x3fu, 0xfcu, 0x3fu, 0xfdu, 0x3fu, 0xfeu, 0x3fu, 0xffu, 0x3fu, 0xffu,
0x3fu, 0xffu, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x00u, 0x04u, 0x01u, 0x08u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x3fu, 0xfeu, 0x3fu,
0xfcu, 0x3fu, 0xf8u, 0x3fu, 0xf2u, 0x3fu, 0xe7u, 0x3fu, 0xd5u, 0x3fu, 0xb7u,
0x3fu, 0x85u, 0x3fu, 0x37u, 0x3eu, 0xbcu, 0x3du, 0xffu, 0x3cu, 0xe4u, 0x3bu,
0x47u, 0x39u, 0x00u, 0x35u, 0xeau, 0x31u, 0xecu, 0x2du, 0x0cu, 0x27u, 0x78u,
0x21u, 0x83u, 0x1bu, 0x97u, 0x16u, 0x14u, 0x11u, 0x41u, 0x0du, 0x3cu, 0x0au,
0x03u, 0x07u, 0x82u, 0x05u, 0x99u, 0x04u, 0x2au, 0x03u, 0x19u, 0x02u, 0x4fu,
0x01u, 0xb9u, 0x01u, 0x4au, 0x00u, 0xf8u, 0x00u, 0xbbu, 0x00u, 0x8eu, 0x00u,
0x6cu, 0x00u, 0x52u, 0x00u, 0x3fu, 0x00u, 0x31u, 0x00u, 0x26u, 0x00u, 0x1du,
0x00u, 0x17u, 0x00u, 0x12u, 0x00u, 0x0eu, 0x00u, 0x0bu, 0x00u, 0x09u, 0x00u,
0x07u, 0x00u, 0x05u, 0x00u, 0x04u, 0x00u, 0x04u, 0x00u, 0x03u, 0x00u, 0x02u,
0x00u, 0x02u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u,
0x08u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x00u, 0x01u,
0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x02u, 0x00u,
0x02u, 0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x04u, 0x00u, 0x05u, 0x00u, 0x07u,
0x00u, 0x08u, 0x00u, 0x0au, 0x00u, 0x0du, 0x00u, 0x11u, 0x00u, 0x15u, 0x00u,
0x1bu, 0x00u, 0x23u, 0x00u, 0x2eu, 0x00u, 0x3bu, 0x00u, 0x4du, 0x00u, 0x65u,
0x00u, 0x84u, 0x00u, 0xafu, 0x00u, 0xe7u, 0x01u, 0x33u, 0x01u, 0x9au, 0x02u,
0x25u, 0x02u, 0xe1u, 0x03u, 0xdfu, 0x05u, 0x34u, 0x06u, 0xfbu, 0x09u, 0x54u,
0x0cu, 0x5du, 0x10u, 0x2eu, 0x14u, 0xd0u, 0x1au, 0x2cu, 0x20u, 0x06u, 0x26u,
0x03u, 0x2bu, 0xb8u, 0x30u, 0xcbu, 0x35u, 0x02u, 0x38u, 0x51u, 0x3au, 0xc8u,
0x3cu, 0x8bu, 0x3du, 0xc3u, 0x3eu, 0x94u, 0x3fu, 0x1du, 0x3fu, 0x75u, 0x3fu,
0xacu, 0x3fu, 0xcfu, 0x3fu, 0xe3u, 0x3fu, 0xf0u, 0x3fu, 0xf7u, 0x3fu, 0xfbu,
0x3fu, 0xfdu, 0x3fu, 0xffu, 0x3fu, 0xffu, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x3fu, 0xffu, 0x3fu,
0xfeu, 0x3fu, 0xfcu, 0x3fu, 0xf9u, 0x3fu, 0xf4u, 0x3fu, 0xeau, 0x3fu, 0xdau,
0x3fu, 0xc0u, 0x3fu, 0x94u, 0x3fu, 0x4eu, 0x3eu, 0xdfu, 0x3eu, 0x35u, 0x3du,
0x34u, 0x3bu, 0xbbu, 0x39u, 0xa3u, 0x36u, 0xc3u, 0x32u, 0xffu, 0x2eu, 0x55u,
0x28u, 0xe7u, 0x23u, 0x01u, 0x1du, 0x08u, 0x17u, 0x64u, 0x12u, 0x61u, 0x0eu,
0x27u, 0x0au, 0xbdu, 0x08u, 0x11u, 0x06u, 0x06u, 0x04u, 0x7bu, 0x03u, 0x55u,
0x02u, 0x7bu, 0x01u, 0xdau, 0x01u, 0x62u, 0x01u, 0x0au, 0x00u, 0xc9u, 0x00u,
0x98u, 0x00u, 0x73u, 0x00u, 0x58u, 0x00u, 0x43u, 0x00u, 0x34u, 0x00u, 0x28u,
0x00u, 0x1fu, 0x00u, 0x18u, 0x00u, 0x13u, 0x00u, 0x0fu, 0x00u, 0x0cu, 0x00u,
0x09u, 0x00u, 0x07u, 0x00u, 0x06u, 0x00u, 0x05u, 0x00u, 0x04u, 0x00u, 0x03u,
0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x08u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x02u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x03u, 0x00u, 0x04u, 0x00u, 0x05u,
0x00u, 0x06u, 0x00u, 0x07u, 0x00u, 0x09u, 0x00u, 0x0cu, 0x00u, 0x0fu, 0x00u,
0x13u, 0x00u, 0x18u, 0x00u, 0x1fu, 0x00u, 0x28u, 0x00u, 0x34u, 0x00u, 0x43u,
0x00u, 0x58u, 0x00u, 0x73u, 0x00u, 0x98u, 0x00u, 0xc9u, 0x01u, 0x0au, 0x01u,
0x62u, 0x01u, 0xdau, 0x02u, 0x7bu, 0x03u, 0x55u, 0x04u, 0x7bu, 0x06u, 0x06u,
0x08u, 0x11u, 0x0au, 0xbdu, 0x0eu, 0x27u, 0x12u, 0x61u, 0x17u, 0x64u, 0x1du,
0x08u, 0x23u, 0x01u, 0x28u, 0xe7u, 0x2eu, 0x55u, 0x32u, 0xffu, 0x36u, 0xc3u,
0x39u, 0xa3u, 0x3bu, 0xbbu, 0x3du, 0x34u, 0x3eu, 0x35u, 0x3eu, 0xdfu, 0x3fu,
0x4eu, 0x3fu, 0x94u, 0x3fu, 0xc0u, 0x3fu, 0xdau, 0x3fu, 0xeau, 0x3fu, 0xf4u,
0x3fu, 0xf9u, 0x3fu, 0xfcu, 0x3fu, 0xfeu, 0x3fu, 0xffu, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x3fu,
0xffu, 0x3fu, 0xffu, 0x3fu, 0xfdu, 0x3fu, 0xfbu, 0x3fu, 0xf7u, 0x3fu, 0xf0u,
0x3fu, 0xe3u, 0x3fu, 0xcfu, 0x3fu, 0xacu, 0x3fu, 0x75u, 0x3fu, 0x1du, 0x3eu,
0x94u, 0x3du, 0xc3u, 0x3cu, 0x8bu, 0x3au, 0xc8u, 0x38u, 0x51u, 0x35u, 0x02u,
0x30u, 0xcbu, 0x2bu, 0xb8u, 0x26u, 0x03u, 0x20u, 0x06u, 0x1au, 0x2cu, 0x14u,
0xd0u, 0x10u, 0x2eu, 0x0cu, 0x5du, 0x09u, 0x54u, 0x06u, 0xfbu, 0x05u, 0x34u,
0x03u, 0xdfu, 0x02u, 0xe1u, 0x02u, 0x25u, 0x01u, 0x9au, 0x01u, 0x33u, 0x00u,
0xe7u, 0x00u, 0xafu, 0x00u, 0x84u, 0x00u, 0x65u, 0x00u, 0x4du, 0x00u, 0x3bu,
0x00u, 0x2eu, 0x00u, 0x23u, 0x00u, 0x1bu, 0x00u, 0x15u, 0x00u, 0x11u, 0x00u,
0x0du, 0x00u, 0x0au, 0x00u, 0x08u, 0x00u, 0x07u, 0x00u, 0x05u, 0x00u, 0x04u,
0x00u, 0x03u, 0x00u, 0x03u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x08u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
0x00u, 0x00u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u, 0x01u, 0x00u,
0x01u, 0x00u, 0x01u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x03u, 0x00u, 0x04u,
0x00u, 0x04u, 0x00u, 0x05u, 0x00u, 0x07u, 0x00u, 0x09u, 0x00u, 0x0bu, 0x00u,
0x0eu, 0x00u, 0x12u, 0x00u, 0x17u, 0x00u, 0x1du, 0x00u, 0x26u, 0x00u, 0x31u,
0x00u, 0x3fu, 0x00u, 0x52u, 0x00u, 0x6cu, 0x00u, 0x8eu, 0x00u, 0xbbu, 0x00u,
0xf8u, 0x01u, 0x4au, 0x01u, 0xb9u, 0x02u, 0x4fu, 0x03u, 0x19u, 0x04u, 0x2au,
0x05u, 0x99u, 0x07u, 0x82u, 0x0au, 0x03u, 0x0du, 0x3cu, 0x11u, 0x41u, 0x16u,
0x14u, 0x1bu, 0x97u, 0x21u, 0x83u, 0x27u, 0x78u, 0x2du, 0x0cu, 0x31u, 0xecu,
0x35u, 0xeau, 0x39u, 0x00u, 0x3bu, 0x47u, 0x3cu, 0xe4u, 0x3du, 0xffu, 0x3eu,
0xbcu, 0x3fu, 0x37u, 0x3fu, 0x85u, 0x3fu, 0xb7u, 0x3fu, 0xd5u, 0x3fu, 0xe7u,
0x3fu, 0xf2u, 0x3fu, 0xf8u, 0x3fu, 0xfcu, 0x3fu, 0xfeu, 0x3fu, 0xffu, 0x3fu,
0xffu, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u,
0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u, 0x00u, 0x40u,
0x00u, 0x00u, 0x00u, 0x00u, 0x08u, 0x00u, 0x04u, 0x00u, 0x01u, 0x01u, 0x00u,
0x00u, 0x01u, 0x02u, 0x01u, 0x00u, 0x02u, 0x01u, 0x02u, 0x00u, 0x02u, 0x02u,
0x03u
};

int main(void)
{
	GPIOB->PDDR |= 0x0000000F;
		GPIOB->PCOR |= 0x0000000F;
		GPIOB->PSOR |= 0x0000000F;
		GPIOB->PCOR |= 0x0000000F;
		GPIOB->PCOR |= 0x0000000F;
	/* Puntero al controlador. */
	uF_controller_dt *controller;
	/* Variables de entrada. */
	uF_fixed_point_dt inputs[2];
	/* Variables de salida. */
	uF_fixed_point_dt outputs[1];


	/* Inicializar el controlador. */
	controller = uF_ufuzInit(cfs_data, sizeof(cfs_data));

	/* Si se inicializó satisfactoriamente: */
	if (controller != 0)
	{
		/* Entonces continuar. */

		GPIOB->PSOR |= 0x0000000F;
				GPIOB->PCOR |= 0x0000000F;
				GPIOB->PCOR |= 0x0000000F;
		for(int i = 0; i <= ciclos; i++){
			inputs[0] = uF_fixpFromInt(-4 + 8 * i / ciclos, -5, 5);
			inputs[1] = uF_fixpFromInt(-4 + 8 * i / ciclos, -5, 5);
			/* Procesar. */
			uF_ufuzProcess(controller, inputs, 2, outputs, 1);
			uF_fixpToInt(outputs[0], -5, 5);
		}
	}
	/* Sino: */
	else
	{
		/* Imprimir el error. */
		printf("Ocurrio el error %d\n", uF_ufuzError(0));
	}
	GPIOB->PSOR |= 0x0000000F;
		GPIOB->PCOR |= 0x0000000F;
		GPIOB->PCOR |= 0x0000000F;
	return 0;
}
