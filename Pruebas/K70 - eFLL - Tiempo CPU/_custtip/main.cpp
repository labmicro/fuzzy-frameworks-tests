#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */
#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

    Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* service = new FuzzyInput(1);
    FuzzySet* poor = new FuzzySet(0, 0, 1, 4);
    service->addFuzzySet(poor);
    FuzzySet* good = new FuzzySet(1, 4, 6, 9);
    service->addFuzzySet(good);
    FuzzySet* excellent = new FuzzySet(6, 9, 10, 10);
    service->addFuzzySet(excellent);
    fuzzy->addFuzzyInput(service);

    // FuzzyInput
    FuzzyInput* food = new FuzzyInput(2);
    FuzzySet* rancid = new FuzzySet(0, 0, 1, 3);
    food->addFuzzySet(rancid);
    FuzzySet* delicious = new FuzzySet(7, 9, 9, 10);
    food->addFuzzySet(delicious);
    fuzzy->addFuzzyInput(food);

	// FuzzyOutput
    FuzzyOutput* tip = new FuzzyOutput(1);
    FuzzySet* cheap = new FuzzySet(0, 5, 5, 10);
    tip->addFuzzySet(cheap);
    FuzzySet* average = new FuzzySet(10, 15, 15, 20);
    tip->addFuzzySet(average);
    FuzzySet* generous = new FuzzySet(20, 25, 25, 30);
    tip->addFuzzySet(generous);
    fuzzy->addFuzzyOutput(tip);

    // Regla1
    FuzzyRuleAntecedent* ifServicePoorORFoodRancid = new FuzzyRuleAntecedent();
    ifServicePoorORFoodRancid->joinWithOR(poor, rancid);

    FuzzyRuleConsequent* thenTipCheap = new FuzzyRuleConsequent();
    thenTipCheap->addOutput(cheap);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifServicePoorORFoodRancid, thenTipCheap);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifServiceGood = new FuzzyRuleAntecedent();
    ifServiceGood->joinSingle(good);

    FuzzyRuleConsequent* thenTipAverage = new FuzzyRuleConsequent();
    thenTipAverage->addOutput(average);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifServiceGood, thenTipAverage);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifServiceExellentOrFoodDelicious = new FuzzyRuleAntecedent();
    ifServiceExellentOrFoodDelicious->joinWithOR(excellent, delicious);

    FuzzyRuleConsequent* thenTipGenerous = new FuzzyRuleConsequent();
    thenTipGenerous->addOutput(generous);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifServiceExellentOrFoodDelicious, thenTipGenerous);
    fuzzy->addFuzzyRule(fuzzyRule3);

    ciclosDeclaracion =  KIN1_GetCycleCounter();
	for (int i = 0; i <= ciclos; ++i){
		fuzzy->setInput(1, 0 + (i * 10 / ciclos));
  		fuzzy->setInput(2, 0 + (i * 10 / ciclos));
  		fuzzy->fuzzify();
 		fuzzy->defuzzify(1);
	}
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1){};

    return 0;
}
