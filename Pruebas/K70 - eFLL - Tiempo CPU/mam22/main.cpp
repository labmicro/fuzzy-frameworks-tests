#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */
#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	Fuzzy* fuzzy = new Fuzzy();

	    // FuzzyInput
	    FuzzyInput* angle = new FuzzyInput(1);
	    FuzzySet* small = new FuzzySet(-5, -5, -1, 1);
	    angle->addFuzzySet(small);
	    FuzzySet* big = new FuzzySet(-1, 1, 5, 5);
	    angle->addFuzzySet(big);
	    fuzzy->addFuzzyInput(angle);

	    // FuzzyInput
	    FuzzyInput* velocity = new FuzzyInput(2);
	    FuzzySet* smallV = new FuzzySet(-5, -5, -2, 2);
	    velocity->addFuzzySet(smallV);
	    FuzzySet* bigV = new FuzzySet(-2, 2, 5, 5);
	    velocity->addFuzzySet(bigV);
	    fuzzy->addFuzzyInput(velocity);

		// FuzzyOutput
	    FuzzyOutput* force = new FuzzyOutput(1);
	    FuzzySet* negBig = new FuzzySet(-5, -5, -4, -3);
	    force->addFuzzySet(negBig);
	    FuzzySet* negSmall = new FuzzySet(-4, -3, 0, 1);
	    force->addFuzzySet(negSmall);
	    FuzzySet* posSmall = new FuzzySet(-1, 0, 3, 4);
	    force->addFuzzySet(posSmall);
		FuzzySet* posBig = new FuzzySet(3, 4, 5, 5);
	    force->addFuzzySet(posBig);;
	    fuzzy->addFuzzyOutput(force);

	    // FuzzyOutput
	    FuzzyOutput* force2 = new FuzzyOutput(1);
	    FuzzySet* negBig2 = new FuzzySet(-5, -4, -2, -1);
	    force2->addFuzzySet(negBig2);
	    FuzzySet* negSmall2 = new FuzzySet(-3, -2, 0, 1);
	    force2->addFuzzySet(negSmall2);
	    FuzzySet* posSmall2 = new FuzzySet(-1, 0, 2, 3);
	    force2->addFuzzySet(posSmall2);
	    FuzzySet* posBig2 = new FuzzySet(1, 2, 4, 5);
	    force2->addFuzzySet(posBig2);;
	    fuzzy->addFuzzyOutput(force2);

	    // Regla1
	    FuzzyRuleAntecedent* ifAngleSmallAndVelocitySmall = new FuzzyRuleAntecedent();
	    ifAngleSmallAndVelocitySmall->joinWithAND(small, smallV);

	    FuzzyRuleConsequent* thenForceNegBigAndForce2PosBig = new FuzzyRuleConsequent();
	    thenForceNegBigAndForce2PosBig->addOutput(negBig);
	    thenForceNegBigAndForce2PosBig->addOutput(posBig2);

	    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifAngleSmallAndVelocitySmall, thenForceNegBigAndForce2PosBig);
	    fuzzy->addFuzzyRule(fuzzyRule1);

	    // Regla2
	    FuzzyRuleAntecedent* ifAngleSmallAndVelocityBig = new FuzzyRuleAntecedent();
	    ifAngleSmallAndVelocityBig->joinWithAND(small, bigV);

	    FuzzyRuleConsequent* thenForceNegSmallAndForce2PosSmall = new FuzzyRuleConsequent();
	    thenForceNegSmallAndForce2PosSmall->addOutput(negSmall);
	    thenForceNegSmallAndForce2PosSmall->addOutput(posSmall2);

	    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifAngleSmallAndVelocityBig, thenForceNegSmallAndForce2PosSmall);
	    fuzzy->addFuzzyRule(fuzzyRule2);

	    // Regla3
	    FuzzyRuleAntecedent* ifAngleBigAndVelocitySmall = new FuzzyRuleAntecedent();
	    ifAngleBigAndVelocitySmall->joinWithAND(big, smallV);

	    FuzzyRuleConsequent* thenForcePosSmallAndForce2NegSmall = new FuzzyRuleConsequent();
	    thenForcePosSmallAndForce2NegSmall->addOutput(posSmall);
	    thenForcePosSmallAndForce2NegSmall->addOutput(negSmall2);

	    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifAngleBigAndVelocitySmall, thenForcePosSmallAndForce2NegSmall);
	    fuzzy->addFuzzyRule(fuzzyRule3);

	    // Regla4
	    FuzzyRuleAntecedent* ifAngleBigAndVelocityBig = new FuzzyRuleAntecedent();
	    ifAngleBigAndVelocityBig->joinWithAND(big, bigV);

	    FuzzyRuleConsequent* thenForcePosBigAndNegBig = new FuzzyRuleConsequent();
	    thenForcePosBigAndNegBig->addOutput(posBig);
	    thenForcePosBigAndNegBig->addOutput(negBig2);

	    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifAngleBigAndVelocityBig, thenForcePosBigAndNegBig);
	    fuzzy->addFuzzyRule(fuzzyRule4);

    ciclosDeclaracion =  KIN1_GetCycleCounter();
    for (int i = 0; i <= ciclos; ++i){
            fuzzy->setInput(1, -5 + (i * 5 / ciclos));
            fuzzy->setInput(2, -5 + (i * 5 / ciclos));
            fuzzy->fuzzify();
            fuzzy->defuzzify(1);
            fuzzy->defuzzify(2);
   }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1);

    return 0;
}
