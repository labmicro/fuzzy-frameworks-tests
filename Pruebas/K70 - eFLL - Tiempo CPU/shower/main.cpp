#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */
#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	Fuzzy* fuzzy = new Fuzzy();

	    // FuzzyInput
	    FuzzyInput* temp = new FuzzyInput(1);
	    FuzzySet* cold = new FuzzySet(-30, -30, -15, 0);
	    temp->addFuzzySet(cold);
	    FuzzySet* good = new FuzzySet(-10, 0, 10, 0);
	    temp->addFuzzySet(good);
	    FuzzySet* hot = new FuzzySet(0, 15, 30, 30);
	    temp->addFuzzySet(hot);
	    fuzzy->addFuzzyInput(temp);

	    // FuzzyInput
	    FuzzyInput* flow = new FuzzyInput(2);
	    FuzzySet* soft = new FuzzySet(-3, -3, -0.8, 0);
	    flow->addFuzzySet(soft);
	    FuzzySet* good1 = new FuzzySet(-0.4, 0, 0.4, 0);
	    flow->addFuzzySet(good1);
	    FuzzySet* hard = new FuzzySet(0, 0.8, 3, 3);
	    flow->addFuzzySet(hard);
	    fuzzy->addFuzzyInput(flow);

		// FuzzyOutput
	    FuzzyOutput* cold1 = new FuzzyOutput(1);
	    FuzzySet* closeFastCold = new FuzzySet(-1, -0.6, -0.3, 0);
	    cold1->addFuzzySet(closeFastCold);
	    FuzzySet* closeSlowCold = new FuzzySet(-0.6, -0.3, 0, 0);
	    cold1->addFuzzySet(closeSlowCold);
	    FuzzySet* steadyCold = new FuzzySet(-0.3, 0, 0.3, 0);
	    cold1->addFuzzySet(steadyCold);
		FuzzySet* openSlowCold = new FuzzySet(0, 0.3, 0.6, 0);
	    cold1->addFuzzySet(openSlowCold);
	    FuzzySet* openFastCold = new FuzzySet(0.3, 0.6, 1, 0);
	    cold1->addFuzzySet(openFastCold);
	    fuzzy->addFuzzyOutput(cold1);

	    // FuzzyOutput
	    FuzzyOutput* hot1 = new FuzzyOutput(2);
	    FuzzySet* closeFastHot = new FuzzySet(-1, -0.6, -0.3, 0);
	    hot1->addFuzzySet(closeFastHot);
	    FuzzySet* closeSlowHot = new FuzzySet(-0.6, -0.3, 0, 0);
	    hot1->addFuzzySet(closeSlowHot);
	    FuzzySet* steadyHot = new FuzzySet(-0.3, 0, 0.3, 0);
	    hot1->addFuzzySet(steadyHot);
		FuzzySet* openSlowHot = new FuzzySet(0, 0.3, 0.6, 0);
	    hot1->addFuzzySet(openSlowHot);
	    FuzzySet* openFastHot = new FuzzySet(0.3, 0.6, 1, 0);
	    hot1->addFuzzySet(openFastHot);
	    fuzzy->addFuzzyOutput(hot1);

	    // Regla1
	    FuzzyRuleAntecedent* ifTempColdAndFlowSoft = new FuzzyRuleAntecedent();
	    ifTempColdAndFlowSoft->joinWithAND(cold, soft);

	    FuzzyRuleConsequent* thenCold1OpenSlowAndHot1OpenFast = new FuzzyRuleConsequent();
	    thenCold1OpenSlowAndHot1OpenFast->addOutput(openSlowCold);
	    thenCold1OpenSlowAndHot1OpenFast->addOutput(openFastHot);

	    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifTempColdAndFlowSoft, thenCold1OpenSlowAndHot1OpenFast);
	    fuzzy->addFuzzyRule(fuzzyRule1);

	    // Regla2
	    FuzzyRuleAntecedent* ifTempColdAndFlowGood = new FuzzyRuleAntecedent();
	    ifTempColdAndFlowGood->joinWithAND(cold, good1);

	    FuzzyRuleConsequent* thenCold1CloseSlowAndHot1OpenSlow = new FuzzyRuleConsequent();
	    thenCold1CloseSlowAndHot1OpenSlow->addOutput(closeSlowCold);
	    thenCold1CloseSlowAndHot1OpenSlow->addOutput(openSlowHot);

	    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifTempColdAndFlowGood, thenCold1CloseSlowAndHot1OpenSlow);
	    fuzzy->addFuzzyRule(fuzzyRule2);

	    // Regla3
	    FuzzyRuleAntecedent* ifTempColdAndFlowHard = new FuzzyRuleAntecedent();
	    ifTempColdAndFlowHard->joinWithAND(cold, hard);

	    FuzzyRuleConsequent* thenCold1CloseFastAndHot1CloseSlow = new FuzzyRuleConsequent();
	    thenCold1CloseFastAndHot1CloseSlow->addOutput(closeFastCold);
	    thenCold1CloseFastAndHot1CloseSlow->addOutput(closeSlowHot);

	    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifTempColdAndFlowHard, thenCold1CloseFastAndHot1CloseSlow);
	    fuzzy->addFuzzyRule(fuzzyRule3);

	    // Regla4
	    FuzzyRuleAntecedent* ifTempGoodAndFlowSoft = new FuzzyRuleAntecedent();
	    ifTempGoodAndFlowSoft->joinWithAND(good, soft);

	    FuzzyRuleConsequent* thenCold1OpenSlowAndHot1OpenSlow = new FuzzyRuleConsequent();
	    thenCold1OpenSlowAndHot1OpenSlow->addOutput(openSlowCold);
	    thenCold1OpenSlowAndHot1OpenSlow->addOutput(openSlowHot);

	    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifTempGoodAndFlowSoft, thenCold1OpenSlowAndHot1OpenSlow);
	    fuzzy->addFuzzyRule(fuzzyRule4);

	    // Regla5
	    FuzzyRuleAntecedent* ifTempGoodAndFlowGood = new FuzzyRuleAntecedent();
	    ifTempGoodAndFlowGood->joinWithAND(good, good1);

	    FuzzyRuleConsequent* thenCold1SteadyAndHot1Steady = new FuzzyRuleConsequent();
	    thenCold1SteadyAndHot1Steady->addOutput(steadyCold);
	    thenCold1SteadyAndHot1Steady->addOutput(steadyHot);

	    FuzzyRule* fuzzyRule5 = new FuzzyRule(5, ifTempGoodAndFlowGood, thenCold1SteadyAndHot1Steady);
	    fuzzy->addFuzzyRule(fuzzyRule5);

	    // Regla6
	    FuzzyRuleAntecedent* ifTempGoodAndFlowHard = new FuzzyRuleAntecedent();
	    ifTempGoodAndFlowHard->joinWithAND(good, hard);

	    FuzzyRuleConsequent* thenCold1CloseSlowAndHot1CloseSlow = new FuzzyRuleConsequent();
	    thenCold1CloseSlowAndHot1CloseSlow->addOutput(closeSlowCold);
	    thenCold1CloseSlowAndHot1CloseSlow->addOutput(closeSlowHot);

	    FuzzyRule* fuzzyRule6 = new FuzzyRule(6, ifTempGoodAndFlowHard, thenCold1CloseSlowAndHot1CloseSlow);
	    fuzzy->addFuzzyRule(fuzzyRule6);

	    // Regla7
	    FuzzyRuleAntecedent* ifTempHotAndFlowSoft = new FuzzyRuleAntecedent();
	    ifTempHotAndFlowSoft->joinWithAND(hot, soft);

	    FuzzyRuleConsequent* thenCold1OpenFastAndHot1OpenSlow = new FuzzyRuleConsequent();
	    thenCold1OpenFastAndHot1OpenSlow->addOutput(openFastCold);
	    thenCold1OpenFastAndHot1OpenSlow->addOutput(openSlowHot);

	    FuzzyRule* fuzzyRule7 = new FuzzyRule(7, ifTempHotAndFlowSoft, thenCold1OpenFastAndHot1OpenSlow);
	    fuzzy->addFuzzyRule(fuzzyRule7);

	    // Regla8
	    FuzzyRuleAntecedent* ifTempHotAndFlowGood = new FuzzyRuleAntecedent();
	    ifTempHotAndFlowGood->joinWithAND(hot, good1);

	    FuzzyRuleConsequent* thenCold1OpenSlowAndHot1CloseSlow = new FuzzyRuleConsequent();
	    thenCold1OpenSlowAndHot1CloseSlow->addOutput(openSlowCold);
	    thenCold1OpenSlowAndHot1CloseSlow->addOutput(closeSlowHot);

	    FuzzyRule* fuzzyRule8= new FuzzyRule(8, ifTempHotAndFlowGood, thenCold1OpenSlowAndHot1CloseSlow);
	    fuzzy->addFuzzyRule(fuzzyRule8);

	    // Regla9
	    FuzzyRuleAntecedent* ifTempHotAndFlowHard = new FuzzyRuleAntecedent();
	    ifTempHotAndFlowHard->joinWithAND(hot, hard);

	    FuzzyRuleConsequent* thenCold1CloseSlowAndHot1CloseFast = new FuzzyRuleConsequent();
	    thenCold1CloseSlowAndHot1CloseFast->addOutput(closeSlowCold);
	    thenCold1CloseSlowAndHot1CloseFast->addOutput(closeFastHot);

	    FuzzyRule* fuzzyRule9= new FuzzyRule(9, ifTempHotAndFlowHard, thenCold1CloseSlowAndHot1CloseFast);
	    fuzzy->addFuzzyRule(fuzzyRule9);

    ciclosDeclaracion =  KIN1_GetCycleCounter();
    for (int i = 0; i <= ciclos; ++i){
            fuzzy->setInput(1, -30 + (i * 30 / ciclos));
            fuzzy->setInput(2, -3 + (i * 3 / ciclos));
            fuzzy->fuzzify();
            fuzzy->defuzzify(1);
            fuzzy->defuzzify(2);
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1){};

    return 0;
}
