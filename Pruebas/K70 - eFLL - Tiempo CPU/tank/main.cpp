#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */
#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	Fuzzy* fuzzy = new Fuzzy();

	    // FuzzyInput
	    FuzzyInput* level = new FuzzyInput(1);
	    FuzzySet* high = new FuzzySet(-1, -1, -1, 0);
	    level->addFuzzySet(high);
	    FuzzySet* okay = new FuzzySet(-1, -0.2, 0.2, 1);
	    level->addFuzzySet(okay);
	    FuzzySet* low = new FuzzySet(0, 1, 1, 1);
	    level->addFuzzySet(low);
	    fuzzy->addFuzzyInput(level);

	    // FuzzyInput
	    FuzzyInput* rate = new FuzzyInput(2);
	    FuzzySet* negative = new FuzzySet(-0.1, -0.1, -0.1, 0);
	    rate->addFuzzySet(negative);
	    FuzzySet* none = new FuzzySet(-0.08, -0.02, 0.02, 0.08);
	    rate->addFuzzySet(none);
	    FuzzySet* positive = new FuzzySet(0, 0.1, 0.1, 0.1);
	    rate->addFuzzySet(positive);
	    fuzzy->addFuzzyInput(rate);

		// FuzzyOutput
	    FuzzyOutput* valve = new FuzzyOutput(1);
	    FuzzySet* closeFast = new FuzzySet(-1, -0.9, -0.9, -0.8);
	    valve->addFuzzySet(closeFast);
	    FuzzySet* closeSlow = new FuzzySet(-0.6, -0.5, -0.5, -0.4);
	    valve->addFuzzySet(closeSlow);
	    FuzzySet* noChange = new FuzzySet(-0.1, 0, 0, 0.1);
	    valve->addFuzzySet(noChange);
		FuzzySet* openSlow = new FuzzySet(0.2, 0.3, 0.3, 0.4);
	    valve->addFuzzySet(openSlow);
	    FuzzySet* openFast = new FuzzySet(0.8, 0.9, 0.9, 1);
	    valve->addFuzzySet(openFast);
	    fuzzy->addFuzzyOutput(valve);

	    // Regla1
	    FuzzyRuleAntecedent* ifLevelOkay = new FuzzyRuleAntecedent();
	    ifLevelOkay->joinSingle(okay);

	    FuzzyRuleConsequent* thenValveNoChange = new FuzzyRuleConsequent();
	    thenValveNoChange->addOutput(noChange);

	    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifLevelOkay, thenValveNoChange);
	    fuzzy->addFuzzyRule(fuzzyRule1);

	    // Regla2
	    FuzzyRuleAntecedent* ifLevelLow = new FuzzyRuleAntecedent();
	    ifLevelLow->joinSingle(low);

	    FuzzyRuleConsequent* thenValveOpenFast = new FuzzyRuleConsequent();
	    thenValveOpenFast->addOutput(openFast);

	    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifLevelLow, thenValveOpenFast);
	    fuzzy->addFuzzyRule(fuzzyRule2);

	    // Regla3
	    FuzzyRuleAntecedent* ifLevelHigh = new FuzzyRuleAntecedent();
	    ifLevelHigh->joinSingle(high);

	    FuzzyRuleConsequent* thenValveCloseFast = new FuzzyRuleConsequent();
	    thenValveCloseFast->addOutput(closeFast);

	    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifLevelHigh, thenValveCloseFast);
	    fuzzy->addFuzzyRule(fuzzyRule3);

	    // Regla4
	    FuzzyRuleAntecedent* ifLevelOkayAndRatePositive = new FuzzyRuleAntecedent();
	    ifLevelOkayAndRatePositive->joinWithAND(okay, positive);

	    FuzzyRuleConsequent* thenValveCloseSlow = new FuzzyRuleConsequent();
	    thenValveCloseSlow->addOutput(closeSlow);

	    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifLevelOkayAndRatePositive, thenValveCloseSlow);
	    fuzzy->addFuzzyRule(fuzzyRule4);

	    // Regla5
	    FuzzyRuleAntecedent* ifLevelOkayAndRateNegative = new FuzzyRuleAntecedent();
	    ifLevelOkayAndRateNegative->joinWithAND(okay, negative);

	    FuzzyRuleConsequent* thenValveOpenSlow = new FuzzyRuleConsequent();
	    thenValveOpenSlow->addOutput(openSlow);

	    FuzzyRule* fuzzyRule5 = new FuzzyRule(5, ifLevelOkayAndRateNegative, thenValveOpenSlow);
	    fuzzy->addFuzzyRule(fuzzyRule5);

    ciclosDeclaracion =  KIN1_GetCycleCounter();
    for (int i = 0; i <= ciclos; ++i){
            fuzzy->setInput(1, -1 + (i * 1 / ciclos));
            fuzzy->setInput(2, -0.1 + (i * 0.1 / ciclos));
            fuzzy->fuzzify();
            fuzzy->defuzzify(1);
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1){};

    return 0;
}
