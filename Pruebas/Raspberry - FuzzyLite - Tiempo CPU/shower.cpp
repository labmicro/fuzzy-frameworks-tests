#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;
	Engine* engine = new Engine;
	engine->setName("shower");
	engine->setDescription("");

	InputVariable* temp = new InputVariable;
	temp->setName("temp");
	temp->setDescription("");
	temp->setEnabled(true);
	temp->setRange(-20.000, 20.000);
	temp->setLockValueInRange(false);
	temp->addTerm(new Trapezoid("cold", -30.000, -30.000, -15.000, 0.000));
	temp->addTerm(new Triangle("good", -10.000, 0.000, 10.000, 0.000));
	temp->addTerm(new Trapezoid("hot", 0.000, 15.000, 30.000, 30.000));
	engine->addInputVariable(temp);

	InputVariable* flow = new InputVariable;
	flow->setName("flow");
	flow->setDescription("");
	flow->setEnabled(true);
	flow->setRange(-1.000, 1.000);
	flow->setLockValueInRange(false);
	flow->addTerm(new Trapezoid("soft", -3.000, -3.000, -0.800, 0.000));
	flow->addTerm(new Triangle("good", -0.400, 0.000, 0.400, 0.000));
	flow->addTerm(new Trapezoid("hard", 0.000, 0.800, 3.000, 3.000));
	engine->addInputVariable(flow);

	OutputVariable* cold = new OutputVariable;
	cold->setName("cold");
	cold->setDescription("");
	cold->setEnabled(true);
	cold->setRange(-1.000, 1.000);
	cold->setLockValueInRange(false);
	cold->setAggregation(new Maximum);
	cold->setDefuzzifier(new Centroid(100));
	cold->setDefaultValue(fl::nan);
	cold->setLockPreviousValue(false);
	cold->addTerm(new Triangle("closeFast", -1.000, -0.600, -0.300, 0.000));
	cold->addTerm(new Triangle("closeSlow", -0.600, -0.300, 0.000, 0.000));
	cold->addTerm(new Triangle("steady", -0.300, 0.000, 0.300, 0.000));
	cold->addTerm(new Triangle("openSlow", 0.000, 0.300, 0.600, 0.000));
	cold->addTerm(new Triangle("openFast", 0.300, 0.600, 1.000, 0.000));
	engine->addOutputVariable(cold);

	OutputVariable* hot = new OutputVariable;
	hot->setName("hot");
	hot->setDescription("");
	hot->setEnabled(true);
	hot->setRange(-1.000, 1.000);
	hot->setLockValueInRange(false);
	hot->setAggregation(new Maximum);
	hot->setDefuzzifier(new Centroid(100));
	hot->setDefaultValue(fl::nan);
	hot->setLockPreviousValue(false);
	hot->addTerm(new Triangle("closeFast", -1.000, -0.600, -0.300, 0.000));
	hot->addTerm(new Triangle("closeSlow", -0.600, -0.300, 0.000, 0.000));
	hot->addTerm(new Triangle("steady", -0.300, 0.000, 0.300, 0.000));
	hot->addTerm(new Triangle("openSlow", 0.000, 0.300, 0.600, 0.000));
	hot->addTerm(new Triangle("openFast", 0.300, 0.600, 1.000, 0.000));
	engine->addOutputVariable(hot);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(new Maximum);
	ruleBlock->setImplication(new Minimum);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if temp is cold and flow is soft then cold is openSlow and hot is openFast", engine));
	ruleBlock->addRule(Rule::parse("if temp is cold and flow is good then cold is closeSlow and hot is openSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is cold and flow is hard then cold is closeFast and hot is closeSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is good and flow is soft then cold is openSlow and hot is openSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is good and flow is good then cold is steady and hot is steady", engine));
	ruleBlock->addRule(Rule::parse("if temp is good and flow is hard then cold is closeSlow and hot is closeSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is hot and flow is soft then cold is openFast and hot is openSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is hot and flow is good then cold is openSlow and hot is closeSlow", engine));
	ruleBlock->addRule(Rule::parse("if temp is hot and flow is hard then cold is closeSlow and hot is closeFast", engine));
	engine->addRuleBlock(ruleBlock);

	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		temp->setValue(-20 + (i * 20 / ciclos));
		flow->setValue(-1 + (i * 1 / ciclos));
		engine->process();
		cold->getValue();
		hot->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}

