#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

Engine* engine = new Engine;
engine->setName("sltbu");
engine->setDescription("");

InputVariable* distance = new InputVariable;
distance->setName("distance");
distance->setDescription("");
distance->setEnabled(true);
distance->setRange(0.000, 25.000);
distance->setLockValueInRange(false);
distance->addTerm(new ZShape("near", 1.000, 2.000, 0.000));
distance->addTerm(new SShape("far", 1.000, 2.000, 0.000));
engine->addInputVariable(distance);

InputVariable* control1 = new InputVariable;
control1->setName("control1");
control1->setDescription("");
control1->setEnabled(true);
control1->setRange(-0.785, 0.785);
control1->setLockValueInRange(false);
engine->addInputVariable(control1);

InputVariable* control2 = new InputVariable;
control2->setName("control2");
control2->setDescription("");
control2->setEnabled(true);
control2->setRange(-0.785, 0.785);
control2->setLockValueInRange(false);
engine->addInputVariable(control2);

OutputVariable* control = new OutputVariable;
control->setName("control");
control->setDescription("");
control->setEnabled(true);
control->setRange(-0.785, 0.785);
control->setLockValueInRange(false);
control->setAggregation(new Maximum);
control->setDefuzzifier(new WeightedAverage("Automatic"));
control->setDefaultValue(fl::nan);
control->setLockPreviousValue(false);
control->addTerm(Linear::create("out1mf1", engine, 0.000, 0.000, 1.000, 0.000));
control->addTerm(Linear::create("out1mf2", engine, 0.000, 1.000, 0.000, 0.000));
engine->addOutputVariable(control);

RuleBlock* ruleBlock = new RuleBlock;
ruleBlock->setName("");
ruleBlock->setDescription("");
ruleBlock->setEnabled(true);
ruleBlock->setConjunction(new AlgebraicProduct);
ruleBlock->setDisjunction(new Maximum);
ruleBlock->setImplication(new AlgebraicProduct);
ruleBlock->setActivation(new General);
ruleBlock->addRule(Rule::parse("if distance is near then control is out1mf1", engine));
ruleBlock->addRule(Rule::parse("if distance is far then control is out1mf2", engine));
engine->addRuleBlock(ruleBlock);


	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		distance->setValue(-3 + (i * 3 / ciclos));
		control1->setValue(-0.785 + (i * 0.785 / ciclos));
		control2->setValue(-0.785 + (i * 0.785 / ciclos));
		engine->process();
		control->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}

