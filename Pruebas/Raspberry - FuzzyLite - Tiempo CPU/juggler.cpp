#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("juggler");
	engine->setDescription("");

	InputVariable* xHit = new InputVariable;
	xHit->setName("xHit");
	xHit->setDescription("");
	xHit->setEnabled(true);
	xHit->setRange(-4.000, 4.000);
	xHit->setLockValueInRange(false);
	xHit->addTerm(new Bell("in1mf1", -4.000, 2.000, 4.000, 0.000));
	xHit->addTerm(new Bell("in1mf2", 0.000, 2.000, 4.000, 0.000));
	xHit->addTerm(new Bell("in1mf3", 4.000, 2.000, 4.000, 0.000));
	engine->addInputVariable(xHit);

	InputVariable* projectAngle = new InputVariable;
	projectAngle->setName("projectAngle");
	projectAngle->setDescription("");
	projectAngle->setEnabled(true);
	projectAngle->setRange(0.000, 3.142);
	projectAngle->setLockValueInRange(false);
	projectAngle->addTerm(new Bell("in2mf1", 0.000, 0.785, 4.000, 0.000));
	projectAngle->addTerm(new Bell("in2mf2", 1.571, 0.785, 4.000, 0.000));
	projectAngle->addTerm(new Bell("in2mf3", 3.142, 0.785, 4.000, 0.000));
	engine->addInputVariable(projectAngle);

	OutputVariable* theta = new OutputVariable;
	theta->setName("theta");
	theta->setDescription("");
	theta->setEnabled(true);
	theta->setRange(0.000, 0.000);
	theta->setLockValueInRange(false);
	theta->setAggregation(new Maximum);
	theta->setDefuzzifier(new WeightedAverage("Automatic"));
	theta->setDefaultValue(fl::nan);
	theta->setLockPreviousValue(false);
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
	theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
	theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
	theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
	theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
	engine->addOutputVariable(theta);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new AlgebraicProduct);
	ruleBlock->setDisjunction(new Maximum);
	ruleBlock->setImplication(new AlgebraicProduct);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf1 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf2 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf3 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf1 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf2 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf3 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf1 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf2 then theta is out1mf", engine));
	ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf3 then theta is out1mf", engine));
	engine->addRuleBlock(ruleBlock);

	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		xHit->setValue(-4 + (i * 4 / ciclos));
		projectAngle->setValue(0 + (i * 3.142 / ciclos));
		engine->process();
		theta->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}