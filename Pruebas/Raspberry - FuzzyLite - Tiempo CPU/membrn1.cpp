#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("anfis");
	engine->setDescription("");

	InputVariable* in_n1 = new InputVariable;
	in_n1->setName("in_n1");
	in_n1->setDescription("");
	in_n1->setEnabled(true);
	in_n1->setRange(1.000, 31.000);
	in_n1->setLockValueInRange(false);
	in_n1->addTerm(new Bell("in1mf1", 2.253, 16.220, 5.050));
	in_n1->addTerm(new Bell("in1mf2", 31.260, 15.021, 1.843));
	engine->addInputVariable(in_n1);

	InputVariable* in_n2 = new InputVariable;
	in_n2->setName("in_n2");
	in_n2->setDescription("");
	in_n2->setEnabled(true);
	in_n2->setRange(1.000, 31.000);
	in_n2->setLockValueInRange(false);
	in_n2->addTerm(new Bell("in2mf1", 0.740, 15.021, 1.843));
	in_n2->addTerm(new Bell("in2mf2", 29.747, 16.220, 5.050));
	engine->addInputVariable(in_n2);

	OutputVariable* out1 = new OutputVariable;
	out1->setName("out1");
	out1->setDescription("");
	out1->setEnabled(true);
	out1->setRange(-0.334, 1.000);
	out1->setLockValueInRange(false);
	out1->setAggregation(new Maximum);
	out1->setDefuzzifier(new WeightedAverage("Automatic"));
	out1->setDefaultValue(fl::nan);
	out1->setLockPreviousValue(false);
	out1->addTerm(Linear::create("out1mf1", engine, 0.026, 0.071, -0.615));
	out1->addTerm(Linear::create("out1mf2", engine, -0.036, 0.036, -1.169));
	out1->addTerm(Linear::create("out1mf3", engine, -0.094, 0.094, 2.231));
	out1->addTerm(Linear::create("out1mf4", engine, -0.071, -0.026, 2.479));
	engine->addOutputVariable(out1);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new AlgebraicProduct);
	ruleBlock->setDisjunction(new Maximum);
	ruleBlock->setImplication(new AlgebraicProduct);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if in_n1 is in1mf1 and in_n2 is in2mf1 then out1 is out1mf1", engine));
	ruleBlock->addRule(Rule::parse("if in_n1 is in1mf1 and in_n2 is in2mf2 then out1 is out1mf2", engine));
	ruleBlock->addRule(Rule::parse("if in_n1 is in1mf2 and in_n2 is in2mf1 then out1 is out1mf3", engine));
	ruleBlock->addRule(Rule::parse("if in_n1 is in1mf2 and in_n2 is in2mf2 then out1 is out1mf4", engine));
	engine->addRuleBlock(ruleBlock);

	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		in_n1->setValue(1 + (i * 31 / ciclos));
		in_n2->setValue(1 + (i * 31 / ciclos));
		engine->process();
		out1->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}

