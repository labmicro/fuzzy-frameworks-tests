#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("sugeno");
	engine->setDescription("");

	InputVariable* input = new InputVariable;
	input->setName("input");
	input->setDescription("");
	input->setEnabled(true);
	input->setRange(-5.000, 5.000);
	input->setLockValueInRange(false);
	input->addTerm(new Gaussian("low", -5.000, 4.000));
	input->addTerm(new Gaussian("high", 5.000, 4.000));
	engine->addInputVariable(input);

	OutputVariable* output = new OutputVariable;
	output->setName("output");
	output->setDescription("");
	output->setEnabled(true);
	output->setRange(0.000, 1.000);
	output->setLockValueInRange(false);
	output->setAggregation(new Maximum);
	output->setDefuzzifier(new WeightedAverage("Automatic"));
	output->setDefaultValue(fl::nan);
	output->setLockPreviousValue(false);
	output->addTerm(Linear::create("line1", engine, -1.000, -1.000));
	output->addTerm(Linear::create("line2", engine, 1.000, -1.000));
	engine->addOutputVariable(output);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new AlgebraicProduct);
	ruleBlock->setDisjunction(new AlgebraicSum);
	ruleBlock->setImplication(new Minimum);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if input is low then output is line1", engine));
	ruleBlock->addRule(Rule::parse("if input is high then output is line2", engine));
	engine->addRuleBlock(ruleBlock);

	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		input->setValue(-5 + (i * 5 / ciclos));
		engine->process();
		output->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}
