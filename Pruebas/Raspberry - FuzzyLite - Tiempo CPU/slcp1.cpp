#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("slcp1");
	engine->setDescription("");

	InputVariable* in1 = new InputVariable;
	in1->setName("in1");
	in1->setDescription("");
	in1->setEnabled(true);
	in1->setRange(-0.300, 0.300);
	in1->setLockValueInRange(false);
	engine->addInputVariable(in1);

	InputVariable* in2 = new InputVariable;
	in2->setName("in2");
	in2->setDescription("");
	in2->setEnabled(true);
	in2->setRange(-1.000, 1.000);
	in2->setLockValueInRange(false);
	engine->addInputVariable(in2);

	InputVariable* in3 = new InputVariable;
	in3->setName("in3");
	in3->setDescription("");
	in3->setEnabled(true);
	in3->setRange(-3.000, 3.000);
	in3->setLockValueInRange(false);
	engine->addInputVariable(in3);

	InputVariable* in4 = new InputVariable;
	in4->setName("in4");
	in4->setDescription("");
	in4->setEnabled(true);
	in4->setRange(-3.000, 3.000);
	in4->setLockValueInRange(false);
	engine->addInputVariable(in4);

	InputVariable* in5 = new InputVariable;
	in5->setName("in5");
	in5->setDescription("");
	in5->setEnabled(true);
	in5->setRange(0.500, 1.500);
	in5->setLockValueInRange(false);
	in5->addTerm(new Gaussian("small", 0.500, 0.200, 0.000));
	in5->addTerm(new Gaussian("medium", 1.000, 0.200, 0.000));
	in5->addTerm(new Gaussian("large", 1.500, 0.200, 0.000));
	engine->addInputVariable(in5);

	OutputVariable* out = new OutputVariable;
	out->setName("out");
	out->setDescription("");
	out->setEnabled(true);
	out->setRange(-10.000, 10.000);
	out->setLockValueInRange(false);
	out->setAggregation(new Maximum);
	out->setDefuzzifier(new WeightedAverage("Automatic"));
	out->setDefaultValue(fl::nan);
	out->setLockPreviousValue(false);
	out->addTerm(Linear::create("outmf1", engine, 32.166, 5.835, 3.162, 3.757, 0.000, 0.000));
	out->addTerm(Linear::create("outmf2", engine, 39.012, 9.947, 3.162, 4.269, 0.000, 0.000));
	out->addTerm(Linear::create("outmf3", engine, 45.009, 13.985, 3.162, 4.666, 0.000, 0.000));
	engine->addOutputVariable(out);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new AlgebraicProduct);
	ruleBlock->setDisjunction(new Maximum);
	ruleBlock->setImplication(new AlgebraicProduct);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if in5 is small then out is outmf1", engine));
	ruleBlock->addRule(Rule::parse("if in5 is medium then out is outmf2", engine));
	ruleBlock->addRule(Rule::parse("if in5 is large then out is outmf3", engine));
	engine->addRuleBlock(ruleBlock);


	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		in1->setValue(-0.3 + (i * 0.3 / ciclos));
		in2->setValue(-1 + (i * 1 / ciclos));
		in3->setValue(-3 + (i * 3 / ciclos));
		in4->setValue(-3 + (i * 3 / ciclos));
		in5->setValue(0.5 + (i * 1.5 / ciclos));
		engine->process();
		out->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}

