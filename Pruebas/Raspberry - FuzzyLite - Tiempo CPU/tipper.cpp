#include <fl/Headers.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000
/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char** argv){

	struct timeval t_inicio, t_declaracion, t_datos;
 	gettimeofday(&t_inicio, NULL);

 	using namespace fl;

	Engine* engine = new Engine;
	engine->setName("tipper");
	engine->setDescription("");

	InputVariable* service = new InputVariable;
	service->setName("service");
	service->setDescription("");
	service->setEnabled(true);
	service->setRange(0.000, 10.000);
	service->setLockValueInRange(false);
	service->addTerm(new Gaussian("poor", 0.000, 1.500));
	service->addTerm(new Gaussian("good", 5.000, 1.500));
	service->addTerm(new Gaussian("excellent", 10.000, 1.500));
	engine->addInputVariable(service);

	InputVariable* food = new InputVariable;
	food->setName("food");
	food->setDescription("");
	food->setEnabled(true);
	food->setRange(0.000, 10.000);
	food->setLockValueInRange(false);
	food->addTerm(new Trapezoid("rancid", 0.000, 0.000, 1.000, 3.000));
	food->addTerm(new Trapezoid("delicious", 7.000, 9.000, 10.000, 10.000));
	engine->addInputVariable(food);

	OutputVariable* tip = new OutputVariable;
	tip->setName("tip");
	tip->setDescription("");
	tip->setEnabled(true);
	tip->setRange(0.000, 30.000);
	tip->setLockValueInRange(false);
	tip->setAggregation(new Maximum);
	tip->setDefuzzifier(new Centroid(100));
	tip->setDefaultValue(fl::nan);
	tip->setLockPreviousValue(false);
	tip->addTerm(new Triangle("cheap", 0.000, 5.000, 10.000));
	tip->addTerm(new Triangle("average", 10.000, 15.000, 20.000));
	tip->addTerm(new Triangle("generous", 20.000, 25.000, 30.000));
	engine->addOutputVariable(tip);

	RuleBlock* ruleBlock = new RuleBlock;
	ruleBlock->setName("");
	ruleBlock->setDescription("");
	ruleBlock->setEnabled(true);
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(new Maximum);
	ruleBlock->setImplication(new Minimum);
	ruleBlock->setActivation(new General);
	ruleBlock->addRule(Rule::parse("if service is poor or food is rancid then tip is cheap", engine));
	ruleBlock->addRule(Rule::parse("if service is good then tip is average", engine));
	ruleBlock->addRule(Rule::parse("if service is excellent or food is delicious then tip is generous", engine));
	engine->addRuleBlock(ruleBlock);

	gettimeofday(&t_declaracion, NULL);

	std::string status;
    if (not engine->isReady(&status)) throw Exception("[engine error] engine is not ready:n" + status, FL_AT);
	for (int i = 0; i <= ciclos; ++i){
		service->setValue(0 + (i * 10 / ciclos));
		food->setValue(0 + (i * 10 / ciclos));
		engine->process();
		tip->getValue();
    }

    using namespace std;
	gettimeofday(&t_datos, NULL);
  	cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
  	cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
  	cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;
return 0;
}

