/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>
uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */

/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
  #define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

#define ciclos 1000

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

int main(int argc, char** argv){
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("juggler");
		engine->setDescription("");

		InputVariable* xHit = new InputVariable;
		xHit->setName("xHit");
		xHit->setDescription("");
		xHit->setEnabled(true);
		xHit->setRange(-4.000, 4.000);
		xHit->setLockValueInRange(false);
		xHit->addTerm(new Bell("in1mf1", -4.000, 2.000, 4.000, 0.000));
		xHit->addTerm(new Bell("in1mf2", 0.000, 2.000, 4.000, 0.000));
		xHit->addTerm(new Bell("in1mf3", 4.000, 2.000, 4.000, 0.000));
		engine->addInputVariable(xHit);

		InputVariable* projectAngle = new InputVariable;
		projectAngle->setName("projectAngle");
		projectAngle->setDescription("");
		projectAngle->setEnabled(true);
		projectAngle->setRange(0.000, 3.142);
		projectAngle->setLockValueInRange(false);
		projectAngle->addTerm(new Bell("in2mf1", 0.000, 0.785, 4.000, 0.000));
		projectAngle->addTerm(new Bell("in2mf2", 1.571, 0.785, 4.000, 0.000));
		projectAngle->addTerm(new Bell("in2mf3", 3.142, 0.785, 4.000, 0.000));
		engine->addInputVariable(projectAngle);

		OutputVariable* theta = new OutputVariable;
		theta->setName("theta");
		theta->setDescription("");
		theta->setEnabled(true);
		theta->setRange(0.000, 0.000);
		theta->setLockValueInRange(false);
		theta->setAggregation(new Maximum);
		theta->setDefuzzifier(new WeightedAverage("Automatic"));
		theta->setDefaultValue(fl::nan);
		theta->setLockPreviousValue(false);
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 0.315));
		theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
		theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
		theta->addTerm(Linear::create("out1mf", engine, 0.114, -0.500, 0.785));
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
		theta->addTerm(Linear::create("out1mf", engine, -0.022, -0.500, 1.256));
		engine->addOutputVariable(theta);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf1 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf2 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf1 and projectAngle is in2mf3 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf1 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf2 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf2 and projectAngle is in2mf3 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf1 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf2 then theta is out1mf", engine));
		ruleBlock->addRule(Rule::parse("if xHit is in1mf3 and projectAngle is in2mf3 then theta is out1mf", engine));
		engine->addRuleBlock(ruleBlock);

	ciclosDeclaracion =  KIN1_GetCycleCounter();

	for (int i = 0; i < ciclos; ++i){
		xHit->setValue(-4 + (i * 4 / ciclos));
		projectAngle->setValue(0 + (i * 3.142 / ciclos));
		engine->process();
		theta->getValue();
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
