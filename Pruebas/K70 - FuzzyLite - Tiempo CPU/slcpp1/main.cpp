/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>
uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */

/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
  #define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

#define ciclos 1000

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

int main(int argc, char** argv){
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("slcpp");
		engine->setDescription("");

		InputVariable* in1 = new InputVariable;
		in1->setName("in1");
		in1->setDescription("");
		in1->setEnabled(true);
		in1->setRange(-0.300, 0.300);
		in1->setLockValueInRange(false);
		engine->addInputVariable(in1);

		InputVariable* in2 = new InputVariable;
		in2->setName("in2");
		in2->setDescription("");
		in2->setEnabled(true);
		in2->setRange(-1.000, 1.000);
		in2->setLockValueInRange(false);
		engine->addInputVariable(in2);

		InputVariable* in3 = new InputVariable;
		in3->setName("in3");
		in3->setDescription("");
		in3->setEnabled(true);
		in3->setRange(-3.000, 3.000);
		in3->setLockValueInRange(false);
		engine->addInputVariable(in3);

		InputVariable* in4 = new InputVariable;
		in4->setName("in4");
		in4->setDescription("");
		in4->setEnabled(true);
		in4->setRange(-3.000, 3.000);
		in4->setLockValueInRange(false);
		engine->addInputVariable(in4);

		InputVariable* in5 = new InputVariable;
		in5->setName("in5");
		in5->setDescription("");
		in5->setEnabled(true);
		in5->setRange(-3.000, 3.000);
		in5->setLockValueInRange(false);
		engine->addInputVariable(in5);

		InputVariable* in6 = new InputVariable;
		in6->setName("in6");
		in6->setDescription("");
		in6->setEnabled(true);
		in6->setRange(-3.000, 3.000);
		in6->setLockValueInRange(false);
		engine->addInputVariable(in6);

		InputVariable* pole_length = new InputVariable;
		pole_length->setName("pole_length");
		pole_length->setDescription("");
		pole_length->setEnabled(true);
		pole_length->setRange(0.500, 1.500);
		pole_length->setLockValueInRange(false);
		pole_length->addTerm(new ZShape("mf1", 0.500, 0.600, 0.000));
		pole_length->addTerm(new PiShape("mf2", 0.500, 0.600, 0.600, 0.700));
		pole_length->addTerm(new PiShape("mf3", 0.600, 0.700, 0.700, 0.800));
		pole_length->addTerm(new PiShape("mf4", 0.700, 0.800, 0.800, 0.900));
		pole_length->addTerm(new PiShape("mf5", 0.800, 0.900, 0.900, 1.000));
		pole_length->addTerm(new PiShape("mf6", 0.900, 1.000, 1.000, 1.100));
		pole_length->addTerm(new PiShape("mf7", 1.000, 1.100, 1.100, 1.200));
		pole_length->addTerm(new PiShape("mf8", 1.100, 1.200, 1.200, 1.300));
		pole_length->addTerm(new PiShape("mf9", 1.200, 1.300, 1.300, 1.400));
		pole_length->addTerm(new PiShape("mf10", 1.300, 1.400, 1.400, 1.500));
		pole_length->addTerm(new SShape("mf11", 1.400, 1.500, 0.000));
		engine->addInputVariable(pole_length);

		OutputVariable* out = new OutputVariable;
		out->setName("out");
		out->setDescription("");
		out->setEnabled(true);
		out->setRange(-10.000, 10.000);
		out->setLockValueInRange(false);
		out->setAggregation(new Maximum);
		out->setDefuzzifier(new WeightedAverage("Automatic"));
		out->setDefaultValue(fl::nan);
		out->setLockPreviousValue(false);
		out->addTerm(Linear::create("outmf1", engine, 168.400, 31.000, -188.050, -49.250, -1.000, -2.700, 0.000, 0.000));
		out->addTerm(Linear::create("outmf2", engine, 233.950, 47.190, -254.520, -66.580, -1.000, -2.740, 0.000, 0.000));
		out->addTerm(Linear::create("outmf3", engine, 342.940, 74.730, -364.370, -95.230, -1.000, -2.780, 0.000, 0.000));
		out->addTerm(Linear::create("outmf4", engine, 560.710, 130.670, -582.960, -152.240, -1.000, -2.810, 0.000, 0.000));
		out->addTerm(Linear::create("outmf5", engine, 1213.880, 300.190, -1236.900, -322.800, -1.000, -2.840, 0.000, 0.000));
		out->addTerm(Linear::create("outmf6", engine, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000));
		out->addTerm(Linear::create("outmf7", engine, -1399.120, -382.950, 1374.630, 358.340, -1.000, -2.900, 0.000, 0.000));
		out->addTerm(Linear::create("outmf8", engine, -746.070, -213.420, 720.900, 187.840, -1.000, -2.930, 0.000, 0.000));
		out->addTerm(Linear::create("outmf9", engine, -528.520, -157.460, 502.680, 130.920, -1.000, -2.960, 0.000, 0.000));
		out->addTerm(Linear::create("outmf10", engine, -419.870, -129.890, 393.380, 102.410, -1.000, -2.980, 0.000, 0.000));
		out->addTerm(Linear::create("outmf11", engine, -354.770, -113.680, 327.650, 85.270, -1.000, -3.010, 0.000, 0.000));
		engine->addOutputVariable(out);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if pole_length is mf1 then out is outmf1", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf2 then out is outmf2", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf3 then out is outmf3", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf4 then out is outmf4", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf5 then out is outmf5", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf6 then out is outmf6", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf8 then out is outmf8", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf7 then out is outmf7", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf9 then out is outmf9", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf10 then out is outmf10", engine));
		ruleBlock->addRule(Rule::parse("if pole_length is mf11 then out is outmf11", engine));
		engine->addRuleBlock(ruleBlock);

	ciclosDeclaracion =  KIN1_GetCycleCounter();

	for (int i = 0; i < ciclos; ++i){
		in1->setValue(-0.3 + (i * 0.3 / ciclos));
		in2->setValue(-1 + (i * 1 / ciclos));
		in3->setValue(-3 + (i * 3 / ciclos));
		in4->setValue(-3 + (i * 3 / ciclos));
		in5->setValue(-3 + (i * 3 / ciclos));
		in6->setValue(-3 + (i * 3 / ciclos));
		pole_length->setValue(0.5 + (i * 1.5 / ciclos));
		engine->process();
		out->getValue();
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
