/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>
uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */

/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
  #define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

#define ciclos 1000

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

int main(int argc, char** argv){
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("mam21");
		engine->setDescription("");

		InputVariable* angle = new InputVariable;
		angle->setName("angle");
		angle->setDescription("");
		angle->setEnabled(true);
		angle->setRange(-5.000, 5.000);
		angle->setLockValueInRange(false);
		angle->addTerm(new Bell("small", -5.000, 5.000, 8.000, 0.000));
		angle->addTerm(new Bell("big", 5.000, 5.000, 8.000, 0.000));
		engine->addInputVariable(angle);

		InputVariable* velocity = new InputVariable;
		velocity->setName("velocity");
		velocity->setDescription("");
		velocity->setEnabled(true);
		velocity->setRange(-5.000, 5.000);
		velocity->setLockValueInRange(false);
		velocity->addTerm(new Bell("small", -5.000, 5.000, 2.000, 0.000));
		velocity->addTerm(new Bell("big", 5.000, 5.000, 2.000, 0.000));
		engine->addInputVariable(velocity);

		OutputVariable* force = new OutputVariable;
		force->setName("force");
		force->setDescription("");
		force->setEnabled(true);
		force->setRange(-5.000, 5.000);
		force->setLockValueInRange(false);
		force->setAggregation(new Maximum);
		force->setDefuzzifier(new Centroid(100));
		force->setDefaultValue(fl::nan);
		force->setLockPreviousValue(false);
		force->addTerm(new Bell("negBig", -5.000, 1.670, 8.000, 0.000));
		force->addTerm(new Bell("negSmall", -1.670, 1.670, 8.000, 0.000));
		force->addTerm(new Bell("posSmall", 1.670, 1.670, 8.000, 0.000));
		force->addTerm(new Bell("posBig", 5.000, 1.670, 8.000, 0.000));
		engine->addOutputVariable(force);

		OutputVariable* force2 = new OutputVariable;
		force2->setName("force2");
		force2->setDescription("");
		force2->setEnabled(true);
		force2->setRange(-5.000, 5.000);
		force2->setLockValueInRange(false);
		force2->setAggregation(new Maximum);
		force2->setDefuzzifier(new Centroid(100));
		force2->setDefaultValue(fl::nan);
		force2->setLockPreviousValue(false);
		force2->addTerm(new Bell("negBig2", -3.000, 1.670, 8.000, 0.000));
		force2->addTerm(new Bell("negSmall2", -1.000, 1.670, 8.000, 0.000));
		force2->addTerm(new Bell("posSmall2", 1.000, 1.670, 8.000, 0.000));
		force2->addTerm(new Bell("posBig2", 3.000, 1.670, 8.000, 0.000));
		engine->addOutputVariable(force2);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new Minimum);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new Minimum);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if angle is small and velocity is small then force is negBig and force2 is posBig2", engine));
		ruleBlock->addRule(Rule::parse("if angle is small and velocity is big then force is negSmall and force2 is posSmall2", engine));
		ruleBlock->addRule(Rule::parse("if angle is big and velocity is small then force is posSmall and force2 is negSmall2", engine));
		ruleBlock->addRule(Rule::parse("if angle is big and velocity is big then force is posBig and force2 is negBig2", engine));
		engine->addRuleBlock(ruleBlock);

	ciclosDeclaracion =  KIN1_GetCycleCounter();

	for (int i = 0; i < ciclos; ++i){
		angle->setValue(-5 + (i * 5 / ciclos));
		velocity->setValue(-5 + (i * 5 / ciclos));
		engine->process();
		force->getValue();
		force2->getValue();
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
