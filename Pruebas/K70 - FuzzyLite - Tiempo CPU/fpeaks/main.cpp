/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <fl/Headers.h>
#include <iostream>
uint32_t ciclosDeclaracion; /* number of cycles */
uint32_t ciclosIteracion; /* number of cycles */

/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
  #define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
    /*!< DWT Control register */
  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
    /*!< CYCCNTENA bit in DWT_CONTROL register */
  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
    /*!< DWT Cycle Counter register */
  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
    /*!< DEMCR: Debug Exception and Monitor Control Register */
  #define KIN1_TRCENA_BIT              (1UL<<24)
    /*!< Trace enable bit in DEMCR register */
#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT
  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */

#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */

#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT
  /*!< Read cycle counter register */

#define ciclos 1000

extern unsigned int __HeapBase;
extern unsigned int __HeapLimit;
extern unsigned int __StackTop;
extern unsigned int __StackLimit;

int main(int argc, char** argv){
	uint32_t* BaseHeap = (uint32_t*) &__HeapBase;
	uint32_t* FinalHeap =  (uint32_t*) &__HeapLimit;
	uint32_t* BaseStack = (uint32_t*) &__StackTop;
	uint32_t* FinalStack = (uint32_t*) &__StackLimit;

	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */

	using namespace fl;

		Engine* engine = new Engine;
		engine->setName("peaks");
		engine->setDescription("");

		InputVariable* in1 = new InputVariable;
		in1->setName("in1");
		in1->setDescription("");
		in1->setEnabled(true);
		in1->setRange(-3.000, 3.000);
		in1->setLockValueInRange(false);
		in1->addTerm(new Bell("in1mf1", -2.233, 1.578, 2.151));
		in1->addTerm(new Bell("in1mf2", -0.394, 0.753, 1.838));
		in1->addTerm(new Bell("in1mf3", 0.497, 0.689, 1.844));
		in1->addTerm(new Bell("in1mf4", 2.270, 1.528, 2.156));
		engine->addInputVariable(in1);

		InputVariable* in2 = new InputVariable;
		in2->setName("in2");
		in2->setDescription("");
		in2->setEnabled(true);
		in2->setRange(-3.000, 3.000);
		in2->setLockValueInRange(false);
		in2->addTerm(new Bell("in1mf1", -2.686, 1.267, 2.044));
		in2->addTerm(new Bell("in1mf2", -0.836, 1.266, 1.796));
		in2->addTerm(new Bell("in1mf3", 0.859, 1.314, 1.937));
		in2->addTerm(new Bell("in1mf4", 2.727, 1.214, 2.047));
		engine->addInputVariable(in2);

		OutputVariable* out1 = new OutputVariable;
		out1->setName("out1");
		out1->setDescription("");
		out1->setEnabled(true);
		out1->setRange(-10.000, 10.000);
		out1->setLockValueInRange(false);
		out1->setAggregation(new Maximum);
		out1->setDefuzzifier(new WeightedAverage("Automatic"));
		out1->setDefaultValue(fl::nan);
		out1->setLockPreviousValue(false);
		out1->addTerm(Linear::create("out1mf1", engine, 0.155, -2.228, -8.974));
		out1->addTerm(Linear::create("out1mf2", engine, -0.312, -7.705, -9.055));
		out1->addTerm(Linear::create("out1mf3", engine, -0.454, -4.437, 6.930));
		out1->addTerm(Linear::create("out1mf4", engine, 0.248, -1.122, 5.081));
		out1->addTerm(Linear::create("out1mf5", engine, -6.278, 25.211, 99.148));
		out1->addTerm(Linear::create("out1mf6", engine, 5.531, 105.916, 157.283));
		out1->addTerm(Linear::create("out1mf7", engine, 19.519, 112.333, -127.796));
		out1->addTerm(Linear::create("out1mf8", engine, -5.079, 34.738, -143.414));
		out1->addTerm(Linear::create("out1mf9", engine, -5.889, 27.311, 116.585));
		out1->addTerm(Linear::create("out1mf10", engine, 21.517, 97.266, 93.802));
		out1->addTerm(Linear::create("out1mf11", engine, 9.198, 79.853, -118.482));
		out1->addTerm(Linear::create("out1mf12", engine, -6.571, 23.026, -87.747));
		out1->addTerm(Linear::create("out1mf13", engine, 0.092, -1.126, -4.527));
		out1->addTerm(Linear::create("out1mf14", engine, -0.304, -4.434, -6.561));
		out1->addTerm(Linear::create("out1mf15", engine, -0.166, -6.284, 7.307));
		out1->addTerm(Linear::create("out1mf16", engine, 0.107, -2.028, 8.159));
		engine->addOutputVariable(out1);

		RuleBlock* ruleBlock = new RuleBlock;
		ruleBlock->setName("");
		ruleBlock->setDescription("");
		ruleBlock->setEnabled(true);
		ruleBlock->setConjunction(new AlgebraicProduct);
		ruleBlock->setDisjunction(new Maximum);
		ruleBlock->setImplication(new AlgebraicProduct);
		ruleBlock->setActivation(new General);
		ruleBlock->addRule(Rule::parse("if in1 is in1mf1 and in2 is in1mf1 then out1 is out1mf1", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf1 and in2 is in1mf2 then out1 is out1mf2", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf1 and in2 is in1mf3 then out1 is out1mf3", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf1 and in2 is in1mf4 then out1 is out1mf4", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf2 and in2 is in1mf1 then out1 is out1mf5", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf2 and in2 is in1mf2 then out1 is out1mf6", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf2 and in2 is in1mf3 then out1 is out1mf7", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf2 and in2 is in1mf4 then out1 is out1mf8", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf3 and in2 is in1mf1 then out1 is out1mf9", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf3 and in2 is in1mf2 then out1 is out1mf10", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf3 and in2 is in1mf3 then out1 is out1mf11", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf3 and in2 is in1mf4 then out1 is out1mf12", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf4 and in2 is in1mf1 then out1 is out1mf13", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf4 and in2 is in1mf2 then out1 is out1mf14", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf4 and in2 is in1mf3 then out1 is out1mf15", engine));
		ruleBlock->addRule(Rule::parse("if in1 is in1mf4 and in2 is in1mf4 then out1 is out1mf16", engine));
		engine->addRuleBlock(ruleBlock);

	ciclosDeclaracion =  KIN1_GetCycleCounter();

	for (int i = 0; i < ciclos; ++i){
		in1->setValue(-3 + (i * 3 / ciclos));
		in2->setValue(-3 + (i * 3 / ciclos));
		engine->process();
		out1->getValue();
    }
	ciclosIteracion = KIN1_GetCycleCounter() - ciclosDeclaracion; /* get cycle counter */
	KIN1_DisableCycleCounter(); /* disable counting if not used any more */
	while(1);
return 0;
}


////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
