#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000

using namespace std;

double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char *argv[]) {
    struct timeval t_inicio, t_declaracion, t_datos;
    gettimeofday(&t_inicio, NULL);

    Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* service = new FuzzyInput(1);
    FuzzySet* poor = new FuzzySet(0, 0, 1, 4);
    service->addFuzzySet(poor);
    FuzzySet* good = new FuzzySet(1, 4, 6, 9);
    service->addFuzzySet(good);
    FuzzySet* excellent = new FuzzySet(6, 9, 10, 10);
    service->addFuzzySet(excellent);
    fuzzy->addFuzzyInput(service);

    // FuzzyOutput
    FuzzyOutput* tip = new FuzzyOutput(1);
    FuzzySet* cheap = new FuzzySet(0, 5, 5, 10);
    tip->addFuzzySet(cheap);
    FuzzySet* average = new FuzzySet(10, 15, 15, 20);
    tip->addFuzzySet(average);
    FuzzySet* generous = new FuzzySet(20, 25, 25, 30);
    tip->addFuzzySet(generous);
    fuzzy->addFuzzyOutput(tip);

    // Regla1
    FuzzyRuleAntecedent* ifServicePoor = new FuzzyRuleAntecedent();
    ifServicePoor->joinSingle(poor);

    FuzzyRuleConsequent* thenTipCheap = new FuzzyRuleConsequent();
    thenTipCheap->addOutput(cheap);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifServicePoor, thenTipCheap);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifServiceGood = new FuzzyRuleAntecedent();
    ifServiceGood->joinSingle(good);

    FuzzyRuleConsequent* thenTipAverage = new FuzzyRuleConsequent();
    thenTipAverage->addOutput(average);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifServiceGood, thenTipAverage);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifServiceExellent = new FuzzyRuleAntecedent();
    ifServiceExellent->joinSingle  (excellent);

    FuzzyRuleConsequent* thenTipGenerous = new FuzzyRuleConsequent();
    thenTipGenerous->addOutput(generous);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifServiceExellent, thenTipGenerous);
    fuzzy->addFuzzyRule(fuzzyRule3);

    gettimeofday(&t_declaracion, NULL);

    for (int i = 0; i <= ciclos; ++i){
        fuzzy->setInput(1, 0 + (i * 10 / ciclos));
        fuzzy->fuzzify();
        fuzzy->defuzzify(1);
    }

    using namespace std;
    gettimeofday(&t_datos, NULL);
    cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
    cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
    cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;

    return 0;
}