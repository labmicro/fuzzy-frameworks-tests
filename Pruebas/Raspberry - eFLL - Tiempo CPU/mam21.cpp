#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#define ciclos 50000

using namespace std;

double timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

int main(int argc, char *argv[]) {
    struct timeval t_inicio, t_declaracion, t_datos;
    gettimeofday(&t_inicio, NULL);

    Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* angle = new FuzzyInput(1);
    FuzzySet* small = new FuzzySet(-5, -5, -1, 1);
    angle->addFuzzySet(small);
    FuzzySet* big = new FuzzySet(-1, 1, 5, 5);
    angle->addFuzzySet(big);
    fuzzy->addFuzzyInput(angle);

    // FuzzyInput
    FuzzyInput* velocity = new FuzzyInput(2);
    FuzzySet* smallV = new FuzzySet(-5, -5, -2, 2);
    velocity->addFuzzySet(smallV);
    FuzzySet* bigV = new FuzzySet(-2, 2, 5, 5);
    velocity->addFuzzySet(bigV);
    fuzzy->addFuzzyInput(velocity);

	// FuzzyOutput
    FuzzyOutput* force = new FuzzyOutput(1);
    FuzzySet* negBig = new FuzzySet(-5, -5, -4, -3);
    force->addFuzzySet(negBig);
    FuzzySet* negSmall = new FuzzySet(-4, -3, 0, 1);
    force->addFuzzySet(negSmall);
    FuzzySet* posSmall = new FuzzySet(-1, 0, 3, 4);
    force->addFuzzySet(posSmall);
	FuzzySet* posBig = new FuzzySet(3, 4, 5, 5);
    force->addFuzzySet(posBig);;
    fuzzy->addFuzzyOutput(force);

    // Regla1
    FuzzyRuleAntecedent* ifAngleSmallAndVelocitySmall = new FuzzyRuleAntecedent();
    ifAngleSmallAndVelocitySmall->joinWithAND(small, smallV);

    FuzzyRuleConsequent* thenForceNegBig = new FuzzyRuleConsequent();
    thenForceNegBig->addOutput(negBig);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifAngleSmallAndVelocitySmall, thenForceNegBig);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifAngleSmallAndVelocityBig = new FuzzyRuleAntecedent();
    ifAngleSmallAndVelocityBig->joinWithAND(small, bigV);

    FuzzyRuleConsequent* thenFoceNegSmall = new FuzzyRuleConsequent();
    thenFoceNegSmall->addOutput(negSmall);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifAngleSmallAndVelocityBig, thenFoceNegSmall);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifAngleBigAndVelocitySmall = new FuzzyRuleAntecedent();
    ifAngleBigAndVelocitySmall->joinWithAND(big, smallV);

    FuzzyRuleConsequent* thenForcePosSmall = new FuzzyRuleConsequent();
    thenForcePosSmall->addOutput(posSmall);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifAngleBigAndVelocitySmall, thenForcePosSmall);
    fuzzy->addFuzzyRule(fuzzyRule3);
 
    // Regla4
    FuzzyRuleAntecedent* ifAngleBigAndVelocityBig = new FuzzyRuleAntecedent();
    ifAngleBigAndVelocityBig->joinWithAND(big, bigV);

    FuzzyRuleConsequent* thenForcePosBig = new FuzzyRuleConsequent();
    thenForcePosBig->addOutput(posBig);

    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifAngleBigAndVelocityBig, thenForcePosBig);
    fuzzy->addFuzzyRule(fuzzyRule4);

    gettimeofday(&t_declaracion, NULL);

    for (int i = 0; i <= ciclos; ++i){
        fuzzy->setInput(1, -5 + (i * 5 / ciclos));
        fuzzy->setInput(2, -5 + (i * 5 / ciclos));
        fuzzy->fuzzify();
        fuzzy->defuzzify(1);
    }

    using namespace std;
    gettimeofday(&t_datos, NULL);
    cout << "Tiempo de declaracion: " << timeval_diff(&t_declaracion, &t_inicio) << " s" << endl;
    cout << "Tiempo de ejecucion (" << ciclos << " ciclos): " << timeval_diff(&t_datos, &t_declaracion) << " s" << endl;
    cout << "Tiempo promedio por ciclo: " << timeval_diff(&t_datos, &t_declaracion) / ciclos << " s" << endl;

    return 0;
}