/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MKE02Z2.h"
#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
    Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* angle = new FuzzyInput(1);
    FuzzySet* small = new FuzzySet(-5, -5, -1, 1);
    angle->addFuzzySet(small);
    FuzzySet* big = new FuzzySet(-1, 1, 5, 5);
    angle->addFuzzySet(big);
    fuzzy->addFuzzyInput(angle);

    // FuzzyInput
    FuzzyInput* velocity = new FuzzyInput(2);
    FuzzySet* smallV = new FuzzySet(-5, -5, -2, 2);
    velocity->addFuzzySet(smallV);
    FuzzySet* bigV = new FuzzySet(-2, 2, 5, 5);
    velocity->addFuzzySet(bigV);
    fuzzy->addFuzzyInput(velocity);

	// FuzzyOutput
    FuzzyOutput* force = new FuzzyOutput(1);
    FuzzySet* negBig = new FuzzySet(-5, -5, -4, -3);
    force->addFuzzySet(negBig);
    FuzzySet* negSmall = new FuzzySet(-4, -3, 0, 1);
    force->addFuzzySet(negSmall);
    FuzzySet* posSmall = new FuzzySet(-1, 0, 3, 4);
    force->addFuzzySet(posSmall);
	FuzzySet* posBig = new FuzzySet(3, 4, 5, 5);
    force->addFuzzySet(posBig);;
    fuzzy->addFuzzyOutput(force);

    // FuzzyOutput
    FuzzyOutput* force2 = new FuzzyOutput(1);
    FuzzySet* negBig2 = new FuzzySet(-5, -4, -2, -1);
    force2->addFuzzySet(negBig2);
    FuzzySet* negSmall2 = new FuzzySet(-3, -2, 0, 1);
    force2->addFuzzySet(negSmall2);
    FuzzySet* posSmall2 = new FuzzySet(-1, 0, 2, 3);
    force2->addFuzzySet(posSmall2);
    FuzzySet* posBig2 = new FuzzySet(1, 2, 4, 5);
    force2->addFuzzySet(posBig2);;
    fuzzy->addFuzzyOutput(force2);

    // Regla1
    FuzzyRuleAntecedent* ifAngleSmallAndVelocitySmall = new FuzzyRuleAntecedent();
    ifAngleSmallAndVelocitySmall->joinWithAND(small, smallV);

    FuzzyRuleConsequent* thenForceNegBigAndForce2PosBig = new FuzzyRuleConsequent();
    thenForceNegBigAndForce2PosBig->addOutput(negBig);
    thenForceNegBigAndForce2PosBig->addOutput(posBig2);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifAngleSmallAndVelocitySmall, thenForceNegBigAndForce2PosBig);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifAngleSmallAndVelocityBig = new FuzzyRuleAntecedent();
    ifAngleSmallAndVelocityBig->joinWithAND(small, bigV);

    FuzzyRuleConsequent* thenForceNegSmallAndForce2PosSmall = new FuzzyRuleConsequent();
    thenForceNegSmallAndForce2PosSmall->addOutput(negSmall);
    thenForceNegSmallAndForce2PosSmall->addOutput(posSmall2);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifAngleSmallAndVelocityBig, thenForceNegSmallAndForce2PosSmall);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifAngleBigAndVelocitySmall = new FuzzyRuleAntecedent();
    ifAngleBigAndVelocitySmall->joinWithAND(big, smallV);

    FuzzyRuleConsequent* thenForcePosSmallAndForce2NegSmall = new FuzzyRuleConsequent();
    thenForcePosSmallAndForce2NegSmall->addOutput(posSmall);
    thenForcePosSmallAndForce2NegSmall->addOutput(negSmall2);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifAngleBigAndVelocitySmall, thenForcePosSmallAndForce2NegSmall);
    fuzzy->addFuzzyRule(fuzzyRule3);

    // Regla4
    FuzzyRuleAntecedent* ifAngleBigAndVelocityBig = new FuzzyRuleAntecedent();
    ifAngleBigAndVelocityBig->joinWithAND(big, bigV);

    FuzzyRuleConsequent* thenForcePosBigAndNegBig = new FuzzyRuleConsequent();
    thenForcePosBigAndNegBig->addOutput(posBig);
    thenForcePosBigAndNegBig->addOutput(negBig2);

    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifAngleBigAndVelocityBig, thenForcePosBigAndNegBig);
    fuzzy->addFuzzyRule(fuzzyRule4);

    for (int i = 0; i <= ciclos; ++i){
        fuzzy->setInput(1, -5 + (i * 5 / ciclos));
        fuzzy->setInput(2, -5 + (i * 5 / ciclos));
        fuzzy->fuzzify();
        fuzzy->defuzzify(1);
        fuzzy->defuzzify(2);
    }

    return 0;
}
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
