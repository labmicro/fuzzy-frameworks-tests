/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MKE02Z2.h"
#include <iostream>
#include "Fuzzy.h"
#include "FuzzyComposition.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzyOutput.h"
#include "FuzzyRule.h"
#include "FuzzyRuleAntecedent.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzySet.h"

#define ciclos 50000

using namespace std;

int main(int argc, char *argv[]) {
    Fuzzy* fuzzy = new Fuzzy();

    // FuzzyInput
    FuzzyInput* level = new FuzzyInput(1);
    FuzzySet* high = new FuzzySet(-1, -1, -1, 0);
    level->addFuzzySet(high);
    FuzzySet* okay = new FuzzySet(-1, -0.2, 0.2, 1);
    level->addFuzzySet(okay);
    FuzzySet* low = new FuzzySet(0, 1, 1, 1);
    level->addFuzzySet(low);
    fuzzy->addFuzzyInput(level);

    // FuzzyInput
    FuzzyInput* rate = new FuzzyInput(2);
    FuzzySet* negative = new FuzzySet(-0.1, -0.1, -0.1, 0);
    rate->addFuzzySet(negative);
    FuzzySet* none = new FuzzySet(-0.08, -0.02, 0.02, 0.08);
    rate->addFuzzySet(none);
    FuzzySet* positive = new FuzzySet(0, 0.1, 0.1, 0.1);
    rate->addFuzzySet(positive);
    fuzzy->addFuzzyInput(rate);

	// FuzzyOutput
    FuzzyOutput* valve = new FuzzyOutput(1);
    FuzzySet* closeFast = new FuzzySet(-1, -0.9, -0.9, -0.8);
    valve->addFuzzySet(closeFast);
    FuzzySet* closeSlow = new FuzzySet(-0.6, -0.5, -0.5, -0.4);
    valve->addFuzzySet(closeSlow);
    FuzzySet* noChange = new FuzzySet(-0.1, 0, 0, 0.1);
    valve->addFuzzySet(noChange);
	FuzzySet* openSlow = new FuzzySet(0.2, 0.3, 0.3, 0.4);
    valve->addFuzzySet(openSlow);
    FuzzySet* openFast = new FuzzySet(0.8, 0.9, 0.9, 1);
    valve->addFuzzySet(openFast);
    fuzzy->addFuzzyOutput(valve);

    // Regla1
    FuzzyRuleAntecedent* ifLevelOkay = new FuzzyRuleAntecedent();
    ifLevelOkay->joinSingle(okay);

    FuzzyRuleConsequent* thenValveNoChange = new FuzzyRuleConsequent();
    thenValveNoChange->addOutput(noChange);

    FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifLevelOkay, thenValveNoChange);
    fuzzy->addFuzzyRule(fuzzyRule1);

    // Regla2
    FuzzyRuleAntecedent* ifLevelLow = new FuzzyRuleAntecedent();
    ifLevelLow->joinSingle(low);

    FuzzyRuleConsequent* thenValveOpenFast = new FuzzyRuleConsequent();
    thenValveOpenFast->addOutput(openFast);

    FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifLevelLow, thenValveOpenFast);
    fuzzy->addFuzzyRule(fuzzyRule2);

    // Regla3
    FuzzyRuleAntecedent* ifLevelHigh = new FuzzyRuleAntecedent();
    ifLevelHigh->joinSingle(high);

    FuzzyRuleConsequent* thenValveCloseFast = new FuzzyRuleConsequent();
    thenValveCloseFast->addOutput(closeFast);

    FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifLevelHigh, thenValveCloseFast);
    fuzzy->addFuzzyRule(fuzzyRule3);

    // Regla4
    FuzzyRuleAntecedent* ifLevelOkayAndRatePositive = new FuzzyRuleAntecedent();
    ifLevelOkayAndRatePositive->joinWithAND(okay, positive);

    FuzzyRuleConsequent* thenValveCloseSlow = new FuzzyRuleConsequent();
    thenValveCloseSlow->addOutput(closeSlow);

    FuzzyRule* fuzzyRule4 = new FuzzyRule(4, ifLevelOkayAndRatePositive, thenValveCloseSlow);
    fuzzy->addFuzzyRule(fuzzyRule4);

    // Regla5
    FuzzyRuleAntecedent* ifLevelOkayAndRateNegative = new FuzzyRuleAntecedent();
    ifLevelOkayAndRateNegative->joinWithAND(okay, negative);

    FuzzyRuleConsequent* thenValveOpenSlow = new FuzzyRuleConsequent();
    thenValveOpenSlow->addOutput(openSlow);

    FuzzyRule* fuzzyRule5 = new FuzzyRule(5, ifLevelOkayAndRateNegative, thenValveOpenSlow);
    fuzzy->addFuzzyRule(fuzzyRule5);

    for (int i = 0; i <= ciclos; ++i){
        fuzzy->setInput(1, -1 + (i * 1 / ciclos));
        fuzzy->setInput(2, -0.1 + (i * 0.1 / ciclos));
        fuzzy->fuzzify();
        fuzzy->defuzzify(1);
    }

    return 0;
}
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
