/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK70F12.h"
#include <stdio.h>
#include <stdint.h>

#include "uF_ufuzzy.h"
#include "uF_fixed_point.h"

#define ciclos 50000
/* Estructura CFS donde se especifican las reglas. */
const uint8_t cfs_data[] = {
	0x43u, 0x46u, 0x53u, 0x00u, 0x01u, 0x01u, 0x02u, 0x01u, 0x03u, 0x00u, 0x00u,
0x00u, 0xe0u, 0x00u, 0x00u, 0x00u, 0x06u, 0x66u, 0x1fu, 0xf7u, 0x00u, 0x00u,
0x1bu, 0x32u, 0x1fu, 0xffu, 0x1fu, 0xffu, 0x2fu, 0xffu, 0x00u, 0x00u, 0x20u,
0x07u, 0x39u, 0x98u, 0x3fu, 0xffu, 0x4fu, 0xfeu, 0x02u, 0x00u, 0x00u, 0x00u,
0xf3u, 0x33u, 0x00u, 0x00u, 0x0cu, 0xccu, 0x1fu, 0xffu, 0x00u, 0x00u, 0x1fu,
0xadu, 0x33u, 0x32u, 0x3fu, 0xffu, 0x4cu, 0xcbu, 0x00u, 0x05u, 0x00u, 0x00u,
0x00u, 0x00u, 0x03u, 0x33u, 0x03u, 0x33u, 0x06u, 0x66u, 0x00u, 0x00u, 0x0cu,
0xccu, 0x0fu, 0xffu, 0x0fu, 0xffu, 0x13u, 0x32u, 0x00u, 0x00u, 0x1cu, 0xccu,
0x1fu, 0xffu, 0x1fu, 0xffu, 0x23u, 0x32u, 0x00u, 0x00u, 0x2cu, 0xccu, 0x2fu,
0xffu, 0x2fu, 0xffu, 0x33u, 0x32u, 0x00u, 0x00u, 0x39u, 0x98u, 0x3cu, 0xcbu,
0x3cu, 0xcbu, 0x3fu, 0xffu, 0x01u, 0x00u, 0x00u, 0x08u, 0x00u, 0x04u, 0x00u,
0x03u, 0x00u, 0x04u, 0x00u, 0x01u, 0x00u, 0x00u, 0x00u, 0x02u, 0x02u, 0x01u,
0x00u, 0x02u, 0x01u, 0x03u
};

int main(void)
{
	/* Puntero al controlador. */
	uF_controller_dt *controller;
	/* Variables de entrada. */
	uF_fixed_point_dt inputs[2];
	/* Variables de salida. */
	uF_fixed_point_dt outputs[1];


	/* Inicializar el controlador. */
	controller = uF_ufuzInit(cfs_data, sizeof(cfs_data));
	/* Si se inicializó satisfactoriamente: */
	if (controller != 0)
	{
		/* Entonces continuar. */


		for(int i = 0; i <= ciclos; i++){
			inputs[0] = uF_fixpFromInt(-1 + 1 * i / ciclos, -1, 1);
			inputs[1] = uF_fixpFromInt(-1 + 1 * i / ciclos, -1, 1);
			/* Procesar. */
			uF_ufuzProcess(controller, inputs, 2, outputs, 1);
			uF_fixpToInt(outputs[0], -1, 1);
		}
	}
	/* Sino: */
	else
	{
		/* Imprimir el error. */
		printf("Ocurrio el error %d\n", uF_ufuzError(0));
	}
	return 0;
}
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
